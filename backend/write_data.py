import json
from sqlalchemy import create_engine, String, Integer, Column
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import Session

from sqlalchemy import insert, delete, select
import pandas as pd
import math
import numpy as np
import random

Base = declarative_base()
zip_dict = dict()
city_dict = dict()

class census_tract(Base):
    __tablename__ = "census_tract"
    geo_id = Column("geo_id", String(255), primary_key=True)
    name = Column("name", String(255))
    population = Column("population", Integer)
    zip_code = Column("zip_code", Integer)
    city = Column("city", String(255))
    county = Column("county", String(255))

    def __init__(self, geo_id, name, population, zip_code, city, county):
        self.geo_id = geo_id
        self.name = name
        self.population = population
        self.zip_code = zip_code
        self.city = city
        self.county = county

class trial(Base):
    __tablename__ = "trial"
    trial_id = Column("trial_id", Integer, primary_key=True, autoincrement=True)
    purpose = Column("purpose", String(255))
    title = Column("title", String(8000))
    recruiting_status = Column("recruiting_status", String(255))
    contact_phone = Column("contact_phone", String(255))
    contact_email = Column("contact_email", String(255))
    address = Column("address", String(255))
    city = Column("city", String(255))
    county = Column("county", String(255))

    
    def __init__(self, trial_id, purpose, title, recruiting_status, contact_phone, contact_email, address, city, county):
        self.trial_id = trial_id
        self.purpose = purpose
        self.title = title
        self.recruiting_status = recruiting_status
        self.contact_phone = contact_phone
        self.contact_email = contact_email
        self.address = address
        self.city = city
        self.county = county

class oncologist(Base):
    __tablename__ = "oncologist"
    npi = Column("npi", String(255), primary_key=True)
    name = Column("name", String(255))
    clinic_name = Column("clinic_name", String(255))
    address = Column("address", String(255))
    gender = Column("gender", String(255))
    city = Column("city", String(255))
    zip_code = Column("zip_code", String(255))
    specialization = Column("specialization", String(255))
    phone_number = Column("phone_number", String(255))
    county = Column("county", String(255))


    def __init__(self, npi, name, clinic_name, address, gender, city, zip_code, specialization, phone_number, county):
        self.npi = npi
        self.name = name
        self.clinic_name = clinic_name
        self.address = address
        self.gender = gender
        self.city = city
        self.zip_code = zip_code
        self.specialization = specialization
        self.phone_number = phone_number
        self.county = county

def insert_census(db, legal):
    data = open("result.json", "r").readlines()
    district_data = json.load(open("census_county_divisions.json", "r"))

    district_dict = dict()

    for line in district_data["data"]["geoId/22"]["arcs"]["containedInPlace+"]["nodes"]:
        key =  line['dcid']
        value = line['name']
        district_dict[key] = value

    idx = 0
    session = Session(db)
    while idx < len(data):
        line = json.loads(data[idx])
        idx += 1
        line2 = json.loads(data[idx])
        idx += 1
        line3 = json.loads(data[idx])
        idx += 1
        query = "INSERT INTO census_tract (geo_id, name, population, zip_code, city, county) VALUES (%s, %s, %s, %s, %s, %s)"
        #population = line["byVariable"]["Count_Person"]["byEntity"][0]["orderedFacets"][0]["observations"][0]["value"]
        #print(line)
        current_geo_id = 0
        for geo_id in line["byVariable"]["Count_Person"]["byEntity"]:
            population = line["byVariable"]["Count_Person"]["byEntity"][geo_id]['orderedFacets'][0]['observations'][0]['value']
            current_geo_id = geo_id
            
        if len(legal.keys()) > 0:
            if not (current_geo_id in legal.keys()):
                continue
            #get the name of the district
        #print(population)
        county = ""
        for geo_id in line2["data"]:
            if len(line2['data'][geo_id]['arcs']['containedInPlace']['nodes']) < 2:
                county = line2["data"][geo_id]["arcs"]["containedInPlace"]["nodes"][0]["name"]
            else:
                county = line2["data"][geo_id]["arcs"]["containedInPlace"]["nodes"][1]["name"]
        zip_code = -1
        skip = False
        for geo_id in line2["data"]:
            if len(line2['data'][geo_id]['arcs']['containedInPlace']['nodes']) < 3:
                try:
                    # print(geo_id)
                    overlaps = line3["data"][geo_id]['arcs']['geoOverlaps']['nodes']
                    j = random.randint(0, len(overlaps) - 1)
                    zip_code = overlaps[j]["name"]
                    break
                except:
                    skip = True
                    break
            else:
                zip_code = line2["data"][geo_id]["arcs"]["containedInPlace"]["nodes"][2]["name"]


        if not skip:
            district_name = district_dict[current_geo_id]
            Census_Tract = census_tract(geo_id = current_geo_id, 
                                               name = district_name, 
                                               population = population, 
                                               zip_code = zip_code, 
                                               city = zip_dict[int(zip_code)], 
                                               county = county.lower())
            session.add(Census_Tract)
    session.commit()
def insert_oncologists(db, legal):
    df = pd.read_csv("DAC_NationalDownloadableFile.csv")
    df = df[df['pri_spec'].str.contains('ONCOLOG')]
    
    df = df[df['State'] == 'LA']
    #print(df)
    last_npi = 0
    session = Session(db)
    print(len(df))
    for index, record in df.iterrows():
        if record['NPI'] == last_npi:
            continue
        if len(legal.keys()) > 0:
            if not (str(record['NPI']) in legal.keys()):
                continue
        telephone_number = record['Telephone Number']
        if telephone_number != telephone_number:
            telephone_number = 0
        values = (int(record['NPI']), str(record['frst_nm']) + " " + str(record['lst_nm']), record['Facility Name'], str(record['adr_ln_1']) + " " + str(record['adr_ln_2']), record['gndr'], record['City/Town'], int(str(record['ZIP Code'])[:5]), record['pri_spec'], str(int(telephone_number)))
        temp = []
        #print(values)
        for item in values:
            if item != item:
                temp.append('N/A')
            else: 
                temp.append(item)
        Oncologist = oncologist(npi = temp[0],
                                name = temp[1],
                                clinic_name = temp[2],
                                address = temp[3],
                                gender = temp[4],
                                city = temp[5],
                                zip_code = temp[6],
                                specialization = temp[7],
                                phone_number = temp[8],
                                county = city_dict[temp[5].lower()])
        session.add(Oncologist)
        last_npi = record['NPI']
    session.commit()

def insert_trials(db, legal):
    trial_data = json.load(open("trials.json", "r"))
    session = Session(db)
    i = 0
    for trials in trial_data['data']:
        for site in trials['sites']:
            if site['org_state_or_province'] == 'LA':
                i+=1
                if len(legal.keys()) > 0:
                    if not ((i) in legal.keys()):
                        continue
                Trial = trial(
                                trial_id = i,
                                purpose = 'Treatment',
                                title = site['org_name'] + ' - ' + trials['official_title'],
                                recruiting_status = site['recruitment_status'],
                                contact_phone = site['contact_phone'],
                                contact_email = site['org_email'],
                                address = site['org_address_line_1'],
                                city = site['org_city'],
                                county = city_dict[site['org_city'].lower()])
                session.add(Trial)
    session.commit()

def refresh_tables(db):
    print("dropping")
    Base.metadata.drop_all(db)
    print("dropped")
    Base.metadata.create_all(db)
    print("created")
    
def main():
    mode = input("Enter mode: ")
    df = pd.read_csv("zip_code_database.csv")
    for index, record in df.iterrows():
        zip_dict[record['zip']] = record['primary_city']
        city_dict[record['primary_city'].lower()] = record['county']
    # print(city_dict)
    db_host = "swe-db.cpqbvp1ljtfc.us-east-2.rds.amazonaws.com"
    db_port = "3306"
    db_user = "admin"
    db_password = "adminpassword"
    db_database = "data"
    cnx = create_engine('mysql+pymysql://' + db_user + ":"+ db_password + "@" + db_host + ":" + str(db_port) + "/" + db_database)
    db = cnx.connect()

    # db.autocommit(True)
    #zero means first instantiation
    if mode == "0":
        refresh_tables(db)
        print("refreshed")
        insert_oncologists(cnx, {})
        print("inserted onclogists")
        insert_trials(cnx, {})
        print("inserted trials")
        insert_census(cnx, {})
        print("inserted census")
        
    #not 0 means cleaning data
    else:
    
        stmt = select(census_tract)
        stmt = stmt.join(oncologist, census_tract.county == oncologist.county).join(trial, census_tract.county == trial.county).group_by(census_tract.geo_id)
        result = db.execute(stmt).all()
        legal_tracts = {}
        for row in result:
            legal_tracts[row._asdict()['geo_id']] = 1
        print("tracts ", len(legal_tracts.keys()))
        
        stmt = select(trial).join(oncologist, oncologist.county == trial.county).join(census_tract, trial.county == census_tract.county).group_by(trial.trial_id)
        result = db.execute(stmt).all()
        legal_trials = {}
        for row in result:
            legal_trials[row._asdict()['trial_id']] = 1
        print("trials ", len(legal_trials.keys()))

        stmt = select(oncologist).join(trial, oncologist.county == trial.county).join(census_tract, oncologist.county == census_tract.county).group_by(oncologist.npi)
        result = db.execute(stmt).all()
        legal_oncologists = {}
        for row in result:
            legal_oncologists[row._asdict()['npi']] = 1
        print("oncologists ", len(legal_oncologists.keys()))
        
        refresh_tables(db)
        print("refreshed")
        insert_oncologists(cnx, legal_oncologists)
        print("inserted onclogists")
        insert_trials(cnx, legal_trials)
        print("inserted trials")
        insert_census(cnx, legal_tracts)
        print("inserted census")

    stmt = select(oncologist)
    result = db.execute(stmt).all()
    legal_oncologists = {}
    for row in result:
        legal_oncologists[row._asdict()['npi']] = 1
    print("oncologists ", len(legal_oncologists.keys()))


    stmt = select(census_tract)
    result = db.execute(stmt).all()
    legal_tracts = {}
    for row in result:
        legal_tracts[row._asdict()['geo_id']] = 1
    print("tract ", len(legal_tracts.keys()))


    stmt = select(trial)
    result = db.execute(stmt).all()
    legal_trials = {}
    for row in result:
        legal_trials[row._asdict()['trial_id']] = 1
    print("trial ", len(legal_trials.keys()))
    db.close()
if __name__ == "__main__":
    main()
