import json
from flask import Flask, request, jsonify
from flask_cors import CORS
from sqlalchemy import create_engine, String, Integer, Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import select
from sqlalchemy import or_
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy.dialects.mysql import match
import ast


app = Flask(__name__)
CORS(app)
Base = declarative_base()

def test_census_tract_all():
    assert (get_census_tract_data()  == get_census_tract_data())

class census_tract(Base):
    __tablename__ = "census_tract"
    geo_id = Column("geo_id", String(255), primary_key=True)
    name = Column("name", String(255))
    population = Column("population", Integer)
    zip_code = Column("zip_code", Integer)
    city = Column("city", String(255))
    county = Column("county", String(255))

    def __init__(self, geo_id, name, population, zip_code, city, county):
        self.geo_id = geo_id
        self.name = name #name
        self.population = population #sort
        self.zip_code = zip_code #filter
        self.city = city #filter
        self.county = county #filter

class trial(Base):
    __tablename__ = "trial"
    trial_id = Column("trial_id", Integer, primary_key=True, autoincrement=True)
    purpose = Column("purpose", String(255))
    title = Column("title", String(8000))
    recruiting_status = Column("recruiting_status", String(255))
    contact_phone = Column("contact_phone", String(255))
    contact_email = Column("contact_email", String(255))
    address = Column("address", String(255))
    city = Column("city", String(255))
    county = Column("county", String(255))

    def __init__(self, trial_id, purpose, title, recruiting_status, contact_phone, contact_email, address, city, county):
        self.trial_id = trial_id #sort
        self.purpose = purpose
        self.title = title #sort
        self.recruiting_status = recruiting_status #filter
        self.contact_phone = contact_phone
        self.contact_email = contact_email
        self.address = address
        self.city = city #filter
        self.county = county #filter

class oncologist(Base):
    __tablename__ = "oncologist"
    npi = Column("npi", String(255), primary_key=True)
    name = Column("name", String(255)) #sort
    clinic_name = Column("clinic_name", String(255)) #sort
    address = Column("address", String(255)) 
    gender = Column("gender", String(255)) #filter
    city = Column("city", String(255)) #filter
    zip_code = Column("zip_code", String(255))
    specialization = Column("specialization", String(255))
    phone_number = Column("phone_number", String(255))
    county = Column("county", String(255)) #filter

    def __init__(self, npi, name, clinic_name, address, gender, city, zip_code, specialization, phone_number, county):
        self.npi = npi
        self.name = name
        self.clinic_name = clinic_name
        self.address = address
        self.gender = gender
        self.city = city
        self.zip_code = zip_code
        self.specialization = specialization
        self.phone_number = phone_number
        self.county = county


def establish_db_conn():
  
    try:
        db_host = "swe-db.cpqbvp1ljtfc.us-east-2.rds.amazonaws.com"
        db_port = "3306"
        db_user = "admin"
        db_password = "adminpassword"
        db_database = "data"
        cnx = create_engine('mysql+pymysql://' + db_user + ":"+ db_password + "@" + db_host + ":" + str(db_port) + "/" + db_database)
        conn = cnx.connect()
        session = Session(conn)
        return conn, session
    except:
       print("Cannot connect to db")

@app.route('/')
def hello():
    return 'Hello from Flask Backend!'

def paginate(val, num, size):
    page_num = num
    page_size = size
    if (page_num is None and page_size is None):
        return jsonify(val)
    elif (page_num is None and not page_size is None):
        page_num = 1
    elif (not page_num is None and page_size is None):
        page_size = 10
    elif((int(page_num) - 1) * int(page_size) > len(val)):
        return jsonify([])

    page_num = int(page_num)
    page_size = int(page_size)
    return jsonify(val[(page_num - 1) * page_size: page_num * page_size])

# Route to get all data from the census_tract table
@app.route('/census_tract_all', methods=['GET'])
def get_census_tract_data():
    conn, session = establish_db_conn()
    stmt = select(census_tract)
    stmt = stmt.join(oncologist, census_tract.county == oncologist.county).join(trial, census_tract.county == trial.county).group_by(census_tract.geo_id)
    
    #check if filter value has been specified
    if not request.args.get('filter') is None:
        stmt = stmt.where(census_tract.__dict__[request.args.get('filter')] == request.args.get('filter_value'))

    if not request.args.get('sort') is None:
        if request.args.get('order') == 'desc':
            stmt = stmt.order_by(census_tract.__dict__[request.args.get('sort')].desc())
        else:
            stmt = stmt.order_by(census_tract.__dict__[request.args.get('sort')])

    if not request.args.get('search') is None:
        stmt = stmt.where(or_(col.contains(request.args.get('search')) for col in census_tract.__table__.columns))

    result = conn.execute(stmt).all()
    print(len(result))
    val = []
    for row in result:
        val.append(row._asdict())
    conn.close()
    
    return paginate(val, request.args.get('page_num'), request.args.get('page_size'))

#Route to get a specific census tract
@app.route('/census_tract', methods=['GET'])
def get_census_tract():
    geo_id = request.args.get('geo_id')
    conn, session = establish_db_conn()
    stmt = select(census_tract).where(census_tract.geo_id == geo_id)
    result = conn.execute(stmt).all()
    if len(result) == 0:
        return jsonify({})
    result = result[0]._asdict()
    stmt = select(oncologist.npi).where(oncologist.county == result['county'])
    row = conn.execute(stmt).all()
    result['npi'] =  [r[0] for r in row]
    stmt = select(trial.trial_id).where(trial.county == result['county'])
    row = conn.execute(stmt).all()
    result['trial_id'] = [r[0] for r in row]
    conn.close()
    return jsonify(result)

# Route to get data from the trial table
@app.route('/trial_all', methods=['GET'])
def get_trial_data():
    conn, session = establish_db_conn()
    stmt = select(trial).join(oncologist, oncologist.county == trial.county).join(census_tract, trial.county == census_tract.county).group_by(trial.trial_id)

    if not request.args.get('filter') is None:
        stmt = stmt.where(trial.__dict__[request.args.get('filter')] == request.args.get('filter_value'))

    if not request.args.get('sort') is None:
        if request.args.get('order') == 'desc':
            stmt = stmt.order_by(trial.__dict__[request.args.get('sort')].desc())
        else:
            stmt = stmt.order_by(trial.__dict__[request.args.get('sort')])

    if not request.args.get('search') is None:
        stmt = stmt.where(or_(col.contains(request.args.get('search')) for col in trial.__table__.columns))

    result = conn.execute(stmt).all()
    print(len(result))
    val = []
    for row in result:
        val.append(row._asdict())
    conn.close()
    return paginate(val, request.args.get('page_num'), request.args.get('page_size'))


#Route to get a specific trial
@app.route('/trial', methods=['GET'])
def get_trial():
    trial_id = request.args.get('trial_id')
    conn, session = establish_db_conn()
    stmt = select(trial).where(trial.trial_id == trial_id)
    result = conn.execute(stmt).all()
    if len(result) == 0:
        return jsonify({})
    result = result[0]._asdict()
    stmt = select(oncologist.npi).where(oncologist.county == result['county'])
    row = conn.execute(stmt).all()
    result['npi'] =  [r[0] for r in row]
    stmt = select(census_tract.geo_id).where(census_tract.county == result['county'])
    row = conn.execute(stmt).all()
    result['geo_id'] = [r[0] for r in row]
    conn.close()
    return jsonify(result)
    
# Route to get data from the oncologist table
@app.route('/oncologist_all', methods=['GET'])
def get_oncologist_data():
    conn, session = establish_db_conn()
    stmt = select(oncologist).join(trial, oncologist.county == trial.county).join(census_tract, oncologist.county == census_tract.county).group_by(oncologist.npi)

    if not request.args.get('filter') is None:
        stmt = stmt.where(oncologist.__dict__[request.args.get('filter')] == request.args.get('filter_value'))

    if not request.args.get('sort') is None:
        if request.args.get('order') == 'desc':
            stmt = stmt.order_by(oncologist.__dict__[request.args.get('sort')].desc())
        else:
            stmt = stmt.order_by(oncologist.__dict__[request.args.get('sort')])

    if not request.args.get('search') is None:
        stmt = stmt.where(or_(col.contains(request.args.get('search')) for col in oncologist.__table__.columns))

    result = conn.execute(stmt).all()
    print(len(result))
    val = []
    for row in result:
        val.append(row._asdict())
    print(jsonify(val))
    conn.close()
    return paginate(val, request.args.get('page_num'), request.args.get('page_size'))


#Function to sort the census tracts by population using bubble sort
def sort_census_tract(input_dict):
    vals = []
    for key in input_dict.keys():
        key = str(key)
        vals.append(eval(key))
    flag = True
    while flag:
        flag = False
        i = 0
        while i < len(vals) - 1:
            if vals[i]['population'] < vals[i + 1]['population']:
                temp = vals[i]
                vals[i] = vals[i + 1]
                vals[i + 1] = temp
                flag = True
            i += 1
    return vals

#Function to sort the oncologists by lexical order of the oncologist's clinic names
def sort_oncologist(input_dict):
    vals = []
    for key in input_dict.keys():
        vals.append(eval(key))
    flag = True
    while flag:
        flag = False
        i = 0
        while i < len(vals) - 1:
            print(vals[i])
            if vals[i]['clinic_name'] < vals[i + 1]['clinic_name']:
                temp = vals[i]
                vals[i] = vals[i + 1]
                vals[i + 1] = temp
                flag = True
            i += 1
    return vals

#Function to sort trials by lexical order of the trial titles
def sort_trial(input_dict):
    vals = []
    for key in input_dict.keys():
        vals.append(eval(key))
    flag = True
    while flag:
        flag = False
        i = 0
        while i < len(vals) - 1:
            if vals[i]['title'] < vals[i + 1]['title']:
                temp = vals[i]
                vals[i] = vals[i + 1]
                vals[i + 1] = temp
                flag = True
            i += 1
    return vals

#Create a frequency map of all results that are filtered by the query 
def freq_table(table, whole_query, sort):
    terms = whole_query.split(" ")
    everything = {}
    for row in table:
        perfect = 0
        temp = {}
        for term in terms:
            total = 0
            for cat in row:
                if not type(row[cat]) is str:
                    continue
                print(row[cat])
                if whole_query in row[cat].lower():
                    perfect = -1 * row[cat].lower().count(whole_query.lower())
                total += row[cat].lower().count(term.lower())
            if total > 0:
                temp[term.lower()] = total
        if perfect < 0:
            everything[str(row)] = perfect
        else:
            everything[str(row)] = temp
    sorted_list = sort(everything)
    flag = True
    
    while flag:
        flag = False
        i = 0
        while i < len(sorted_list) - 1:
            if type(everything[str(sorted_list[i + 1])]) is int and type(everything[str(sorted_list[i])]) is dict:
                temp = sorted_list[i]
                sorted_list[i] = sorted_list[i + 1]
                sorted_list[i + 1] = temp
                flag = True
            elif type(everything[str(sorted_list[i + 1])]) is int and type(everything[str(sorted_list[i])]) is int:
                if everything[str(sorted_list[i])] > everything[str(sorted_list[i + 1])]:
                    temp = sorted_list[i]
                    sorted_list[i] = sorted_list[i + 1]
                    sorted_list[i + 1] = temp
                    flag = True
            elif type(everything[str(sorted_list[i + 1])]) is dict and type(everything[str(sorted_list[i])]) is int:
                print("here")
            elif len(everything[str(sorted_list[i])].keys()) < len(everything[str(sorted_list[i + 1])].keys()):
                temp = sorted_list[i]
                sorted_list[i] = sorted_list[i + 1]
                sorted_list[i + 1] = temp
                flag = True
            elif len(everything[str(sorted_list[i])].keys()) == len(everything[str(sorted_list[i + 1])].keys()):
                sum_a = 0
                sum_b = 0
                for val in everything[str(sorted_list[i])].values():
                    sum_a += val
                for val in everything[str(sorted_list[i + 1])].values():
                    sum_b += val
                if sum_a < sum_b:
                    temp = sorted_list[i]
                    sorted_list[i] = sorted_list[i + 1]
                    sorted_list[i + 1] = temp
                    flag = True

            i += 1
    return sorted_list

#Route to return a specific oncologist
@app.route('/oncologist', methods=['GET'])
def get_oncologist():
    npi = request.args.get('npi')
    conn, session = establish_db_conn()
    stmt = select(oncologist).where(oncologist.npi == npi)
    result = conn.execute(stmt).all()
    if len(result) == 0:
        return jsonify({})
    result = result[0]._asdict()
    stmt = select(trial.trial_id).where(trial.county == result['county'])
    row = conn.execute(stmt).all()
    result['trial_id'] =  [r[0] for r in row]
    stmt = select(census_tract.geo_id).where(census_tract.county == result['county'])
    row = conn.execute(stmt).all()
    result['geo_id'] = [r[0] for r in row]
    conn.close()
    return jsonify(result)

def search_census(terms):
    conn, session = establish_db_conn()
    query = []
    return_val = []
    serialized_result = []
    whole_query = " ".join(terms)
    whole_query = whole_query.lower()

    searchable = [census_tract.city, census_tract.county, census_tract.geo_id, census_tract.geo_id, census_tract.name]
    result = session.query(census_tract).filter(or_(text(whole_query)))
    for search_col in searchable:
        result = session.query(census_tract).filter(search_col.contains(whole_query))
        if result.first():
            print("perfect match")
            item_dict = dict(result.first().__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)
    

    for term in terms:
        print(term)
        query.append(census_tract.city.contains(term))
        query.append(census_tract.county.contains(term))
        query.append(census_tract.name.contains(term))
        query.append(census_tract.geo_id.contains(term))
        result = session.query(census_tract).filter(or_(*query))
        
        for item in result.all():
            item_dict = dict(item.__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)
        
    for item in serialized_result:
        print(item)

    return freq_table(serialized_result, whole_query, sort_census_tract)

#Function to search values in the trial model
def search_trials(terms):
    conn, session = establish_db_conn()
    query = []
    return_val = []
    serialized_result = []
    whole_query = " ".join(terms)
    whole_query = whole_query.lower()
    
    searchable = [trial.city, trial.address, trial.contact_email, trial.county, trial.purpose, trial.title]
    result = session.query(trial).filter(or_(text(whole_query)))
    for search_col in searchable:
        result = session.query(trial).filter(search_col.contains(whole_query))
        if result.first():
            item_dict = dict(result.first().__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)

    for term in terms:
        term = term.lower()
        query.append(trial.city.contains(term))
        query.append(trial.county.contains(term))
        query.append(trial.title.contains(term))
        query.append(trial.address.contains(term))
        query.append(trial.contact_email.contains(term))
        
        result = session.query(trial).filter(or_(*query))
        for item in result.all():
            item_dict = dict(item.__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)

    return freq_table(serialized_result, whole_query, sort_trial)


#Function to search the oncologists model
def search_oncologists(terms):

    conn, session = establish_db_conn()
    query = []
    return_val = []
    serialized_result = []


    whole_query = " ".join(terms)
    whole_query = whole_query.lower()
    
    searchable = [oncologist.address, oncologist.city, oncologist.clinic_name, oncologist.county, oncologist.name, oncologist.specialization]
    result = session.query(oncologist).filter(or_(text(whole_query)))
    for search_col in searchable:
        result = session.query(oncologist).filter(search_col.contains(whole_query))
        if result.first():
            print("perfect match")
            item_dict = dict(result.first().__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)
    

    for term in terms:
        term = term.lower()
        query.append(oncologist.name.contains(term))
        query.append(oncologist.address.contains(term))
        query.append(oncologist.clinic_name.contains(term))
        query.append(oncologist.city.contains(term))
        query.append(oncologist.county.contains(term))
        query.append(oncologist.specialization.contains(term))

        result = session.query(oncologist).filter(or_(*query))
        for item in result.all():
            item_dict = dict(item.__dict__)
            del item_dict['_sa_instance_state']
            serialized_result.append(item_dict)

    return freq_table(serialized_result, whole_query, sort_oncologist)

#Route for search requests
@app.route('/search', methods = ['GET'])
def search():
    query_terms = request.args.get('search_val').split("_")
    model = request.args.get('model')
    return_result = None
    match model:
        case "census":
            return_result = search_census(query_terms)
        case "trial":
            return_result = search_trials(query_terms)
        case "oncologist":
            return_result = search_oncologists(query_terms)
            
        case _:
            census_result = search_census(query_terms)
            onc_result = search_oncologists(query_terms)
            trial_result = search_trials(query_terms)
            return_result = {"census tract": census_result, "oncologist": onc_result, "trial": trial_result}
    return jsonify(return_result)

if __name__ == '__main__':
    establish_db_conn()
    app.run(port=5000, debug=True)

