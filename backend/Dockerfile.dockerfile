FROM python:latest

COPY ./requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt
#CMD ["cd", "/usr/python"]
WORKDIR /backend
COPY . ./
EXPOSE 5000
EXPOSE 8080
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "app:app"]
