from app import app
import unittest
import json
from flask import jsonify
class Test(unittest.TestCase):
    def setUp(self):
            self.app = app.test_client()
            self.app.testing = True

    def test_census_tract_all(self):
        response = json.loads(self.app.get('/census_tract_all?page_num=1&page_size=3').data.decode('utf-8'))
        with open('census_tract_all_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            i = 0
            stored = json.load(f)
            for result in response:
                for key in result.keys():
                    self.assertEqual(result[key], stored[i][key])
                i+=1

    def test_census_tract(self):
        response = self.app.get('/census_tract?geo_id=geoId/2201794024').data.decode('utf-8')
        response = json.loads(response)
        with open('census_tract_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            stored = json.load(f)
            for key in stored.keys():
                self.assertEqual(response[key], stored[key])
    
    def test_trial_all(self):
        response = json.loads(self.app.get('/trial_all?page_num=1&page_size=3').data.decode('utf-8'))
        with open('trial_all_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            i = 0
            stored = json.load(f)
            for result in response:
                for key in result.keys():
                    self.assertEqual(result[key], stored[i][key])
                i+=1

    def test_trial(self):
        response = self.app.get('/trial?trial_id=1').data.decode('utf-8')
        response = json.loads(response)
        with open('trial_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            stored = json.load(f)
            for key in stored.keys():
                self.assertEqual(response[key], stored[key])


    def test_oncologist_all(self):
        response = json.loads(self.app.get('/oncologist_all?page_num=1&page_size=3').data.decode('utf-8'))
        with open('oncologist_all_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            i = 0
            stored = json.load(f)
            for result in response:
                for key in result.keys():
                    self.assertEqual(result[key], stored[i][key])
                i+=1
    
    def test_oncologist(self):
        response = self.app.get('/oncologist?npi=1013108141').data.decode('utf-8')
        response = json.loads(response)
        with open('oncologist_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            stored = json.load(f)
            for key in stored.keys():
                self.assertEqual(response[key], stored[key])

    def test_search(self):
        response = json.loads(self.app.get('/search?search_val=cancer&model=all').data.decode('utf-8'))
        with open('search_test.json', 'r') as f:
            # f.write(json.dumps(response))
            # f.close()
            i = 0
            stored = json.load(f)
            self.assertEqual(stored, response)
if __name__ == "__main__":
    unittest.main()
