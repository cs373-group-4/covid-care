# Canvas/Ed Discussion group number: Group 4

## Names of the team members:

- Hursh Jha
- Twila Ternida
- Emme Erwin
- Aryan Parikh
- Truc Tran

## Project Name: cancer-care

## Website Link: https://www.cancer-care.me/

## Backend API Link: https://backend.cancer-care.me/

## API docs Link: https://documenter.getpostman.com/view/11695219/2s9YJjSz2L

## The proposed project:

Our project aims to help provide easier access to healthcare in regards to cancer for people in “Cancer Alley” which is a nickname for the area along the Mississippi River between Baton Rouge and New Orleans. This area accounts for 25% of the petrochemical production in the USA and has significant amounts of pollution. The site will have the locations for oncologists in each city, and information about clinical trials related to cancer happening in the area, for those who want to participate in trials or have access to more information about cancer related studies.

## URLs of data sources to scrape:

1. https://www.cancer.gov/types
2. https://clinicaltrialsapi.cancer.gov/doc
3. ​​​​https://docs.datacommons.org/api/rest/v2/node
4. https://healthdata.ldh.la.gov/​​
5. https://data.cms.gov/provider-data/docs

## Models:

1. Census tracts in the area
2. Oncologists in the area
3. Clinical trials for cancer

### An estimate of the number of instances of each model:

- Census tracts in region: ~250
- Oncologists: ~1000
- Clinical trials: ~200

### Each model must have many attributes. Describe five of those attributes for each model that you can filter or sort:

1. Census tracts

   - County it is in
   - City it is in
   - District
   - Zip code
   - Population
   - Number of oncologists
   - Number of clinical trials

2. Oncologists

   - Specialization (primary / secondary in the same filter)
   - Gender
   - City
   - Facility name
   - Phone number

3. Clinical trials
   - Title of the study
   - Clinic
   - Recruiting status
   - Location
   - City
   - Contact info (phone/email)

### Instances of each model must connect to instances of at least two other models:

1. Census tracts

   - Oncologists in the Census tracts
   - Clinical trials in the Census tracts

2. Oncologist

   - County of center
   - Clinical trials nearby

3. Clinical trials
   - Oncologists nearby
   - Census tracts of trial

### Describe two types of media for instances of each model:

1. Census tracts in Texas

   - Map
   - Image of the area

2. Oncologist

   - Image of the oncologist
   - Map of clinic
   - Text description

3. Clinical trials
   - Map
   - Image
   - Text description

### Describe three questions that your site will answer:

- How can people in a pollution dense area access cancer treatments?
- Where is the closest oncologist to me?
- How can I learn about or participate in studies related to cancer?

### About our team (No Phase Leader)

1. Aryan Parikh (aryan.parikh02@gmail.com)
   - I'm Aryan, a fourth year Computer Science major at UT Austin. In my free time, I love to dance and try out different recipes in the kitchen
2. Emme Erwin (emilyerwin20@gmail.com)
   - I am a senior CS major at UT Austin. I love cats and reading.
3. Hursh Jha (hursh.jha@gmail.com)
   - I'm a third year CS & Math major. I enjoy reading and playing various TCG's.
4. Truc Tran (tructran@utexas.edu)
   - I am a third-year CS major at the University of Texas at Austin. I enjoy listening to music and cooking.
5. Twila Ternida (twilaternida@utexas.edu)
   - I am a fourth-year CS major at UT Austin. I love birds and writing!

### Project Estimation / Timeline

- We expected it to take ~80 hours to complete the project
- This ended up being roughly correct with about 20-25 hours per person +/- some time

### Git SHA - 4775cae138755e43310e123f1501a09308b344e7

### Comments

Credit: we used a Dockerfile for the frontend that was referenced from the AllTheSports project from last year. We also referenced Bridging the Bayou for the Observable Plot visualizations, and Wine World for the Recharts visualizations.
