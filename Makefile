.DEFAULT_GOAL := all
SHELL := bash

# Define the `status` target
status:
	@echo
	git status

.PHONY: status

# Define the `clean` target
clean:
	rm -rf generated_files/

# Get git config
config:
	git config -l

# Download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# Upload files to the Collatz code repo
push:
	make clean
	@echo
	git add -A
	git commit -m "another commit"
	git push 
	git status

# Output versions of all tools
versions:
	uname -p
	@echo
	uname -s
	@echo
	which git
	@echo
	git --version
	@echo
	which make
	@echo
	make --version | head -n 1
	@echo
	which pip
	@echo
	pip --version
	@echo
	which $(PYTHON)
	@echo
	$(PYTHON) --version
	@echo
	which vim
	@echo
	vim --version | head -n 1