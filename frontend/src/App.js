import './App.css'
import { useState } from 'react'
import Cookies from 'js-cookie'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { ThemeProvider, createTheme } from '@mui/material/styles'

import About from './pages/About'
import MainLayout from './layouts/MainLayout.js'
import Oncologists from './pages/Oncologists'
import CensusTracts from './pages/CensusTracts'
import ClinicalTrials from './pages/ClinicalTrials'
import Homepage from './pages/Homepage.js'
import OncologistDetail from './pages/OncologistDetail'
import CountyDetail from './pages/CountyDetail'
import ClinicTrialDetail from './pages/ClinicTrialDetail'
import SearchPage from './pages/SearchPage'
import { ErrorHomepage, ErrorPage } from './pages/ErrorPage'
import Visualizations from './pages/Visualizations.js'
import ProviderVisualizations from './pages/ProviderVisualizations.js'

function App() {
    // used to toggle the light and dark mode
    const [mode, setMode] = useState(Cookies.get('colorMode') || 'light')
    const backgroundColor = mode === 'dark' ? '#121212' : '#fff'
    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
        },
        components: {
            MuiTypography: {
                defaultProps: {
                    color: '#fff',
                },
            },
        },
    })
    const lightTheme = createTheme({
        palette: {
            mode: 'light',
        },
    })

    return (
        <div className="App" style={{ background: backgroundColor }}>
            <ThemeProvider theme={mode === 'dark' ? darkTheme : lightTheme}>
                <Router>
                    <Routes>
                        <Route path="/" element={<Homepage />} />
                        <Route
                            path="/pages/"
                            element={
                                <MainLayout mode={mode} setMode={setMode} />
                            }
                        >
                            <Route path="/pages/About" element={<About />} />
                            <Route
                                path="/pages/Oncologists"
                                element={<Oncologists />}
                            />
                            <Route
                                path="/pages/Oncologists/:id"
                                element={<OncologistDetail />}
                            />
                            <Route
                                path="/pages/ClinicalTrials"
                                element={<ClinicalTrials />}
                            />
                            <Route
                                path="/pages/ClinicalTrials/:id"
                                element={<ClinicTrialDetail />}
                            />
                            <Route
                                path="/pages/CensusTracts"
                                element={<CensusTracts />}
                            />
                            <Route
                                path="/pages/CensusTracts/:id"
                                element={<CountyDetail />}
                            />
                            <Route
                                path="/pages/search/:term"
                                element={<SearchPage />}
                            />
                            <Route
                                path="/pages/Visualizations"
                                element={<Visualizations />}
                            />
                            <Route
                                path="/pages/ProviderVisualizations"
                                element={<ProviderVisualizations />}
                            />
                            <Route path="*" element={<ErrorPage />} />
                        </Route>
                        <Route path="*" element={<ErrorHomepage />} />
                    </Routes>
                </Router>
            </ThemeProvider>
        </div>
    )
}
export default App
