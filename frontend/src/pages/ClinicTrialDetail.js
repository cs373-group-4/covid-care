import { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Box } from '@mui/system'
import { Container, Grid, Typography, Stack } from '@mui/material'
import { BASE_URL } from '../utils/config.js'
import Map from '../components/Map'
import ClinicalTrial from '../components/ClinicalTrial'
import CensusTractCard from '../components/CensusTractCard'
import OncologistCard from '../components/OncologistCard'
import Loading from '../components/Loading.js'

function ClinicTrialDetail() {
    const [data, setData] = useState(null)

    const [address, setAddress] = useState('')
    const [geoIDs, setGeoIDs] = useState(null)
    const [counties, setCounties] = useState([])
    const [loading, setLoading] = useState(false)
    const [oncologistIDs, setOncologistIDs] = useState(null)
    const [oncologists, setOncologists] = useState([])
    const [loading1, setLoading1] = useState(false)

    const { id } = useParams()

    useEffect(() => {
        const fetchData = async () => {
            const base = `${BASE_URL}trial`
            const url = `${base}?trial_id=${id}`

            try {
                console.log(url)
                const response = await fetch(url)
                const responseData = await response.json()
                setData(responseData)
                setAddress(
                    responseData.address +
                        ', ' +
                        responseData.city +
                        ', ' +
                        responseData.county
                )

                setGeoIDs(responseData.geo_id)
                setOncologistIDs(responseData.npi)
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [id])

    useEffect(() => {
        const fetchDataCounty = async () => {
            setLoading1(true)
            const base = `${BASE_URL}census_tract`
            try {
                const fetchedData = []
                for (const id of geoIDs) {
                    const county_id = id.slice(6)
                    const url = `${base}?geo_id=geoID/${county_id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()
                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setCounties(fetchedData)
            } catch (error) {
                console.log(error)
            }
            setLoading1(false)
        }
        fetchDataCounty()
    }, [geoIDs])

    useEffect(() => {
        const fetchDataOncologists = async () => {
            setLoading(true)
            const base = `${BASE_URL}oncologist`
            try {
                const fetchedData = []
                for (const id of oncologistIDs) {
                    const url = `${base}?npi=${id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()
                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setOncologists(fetchedData)
            } catch (error) {
                console.log(error)
            }

            setLoading(false)
        }
        fetchDataOncologists()
    }, [oncologistIDs])

    if (!data) return <Loading />

    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                }}
            >
                <ClinicalTrial clinic={data} />
                <Typography
                    justifyContent={'center'}
                    fontFamily={'Serif'}
                    variant="h5"
                >
                    <p>Location</p>
                </Typography>
                <Stack
                    width="80%"
                    height="400px"
                    sx={{
                        borderRadius: '10px',
                        mt: '10px',
                        mb: '10px',
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        border: '1px solid black',
                        backgroundColor: '#74b9ff',
                    }}
                >
                    <Map address={encodeURIComponent(address)}></Map>
                </Stack>

                <Typography
                    justifyContent={'center'}
                    variant="h5"
                    fontFamily={'Serif'}
                >
                    <p>Explore cities in this area: </p>
                </Typography>
                {!loading && (
                    <Grid container direction="row">
                        {counties.map((county) => (
                            <Grid
                                item
                                xs={12}
                                sm={10}
                                md={3}
                                xl={3}
                                key={county.geo_id}
                            >
                                <CensusTractCard county={county} />
                            </Grid>
                        ))}
                    </Grid>
                )}

                <Typography
                    justifyContent={'center'}
                    variant="h5"
                    fontFamily={'Serif'}
                >
                    <p>Oncologists in this area: </p>
                </Typography>
                {!loading1 && (
                    <Grid container direction="row">
                        {oncologists.map((oncologist) => (
                            <Grid
                                item
                                xs={12}
                                sm={10}
                                md={3}
                                xl={3}
                                key={oncologist.npi}
                            >
                                <OncologistCard oncologist={oncologist} />
                            </Grid>
                        ))}
                    </Grid>
                )}
            </Container>
        </Box>
    )
}

export default ClinicTrialDetail
