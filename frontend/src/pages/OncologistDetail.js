import { useParams } from 'react-router'
import { useEffect, useState } from 'react'
import { Box } from '@mui/system'
import { Container, Stack, Typography, Grid } from '@mui/material'
import { BASE_URL } from '../utils/config.js'
import Map from '../components/Map'
import OncologistInst from '../components/OncologistInst'
import ClinicalTrialCard from '../components/ClinicalTrialCard'
import CensusTractCard from '../components/CensusTractCard'
import Loading from '../components/Loading.js'

function OncologistDetail() {
    const [data, setData] = useState([])
    const [address, setAddress] = useState('')
    const [geoIDs, setGeoIDs] = useState(null)
    const [counties, setCounties] = useState([])
    const [trialIDs, setTrialIDs] = useState(null)
    const [trialList, setTrialList] = useState([])
    const [loading, setLoading] = useState(false)
    const [loading2, setLoading2] = useState(false)

    const { id } = useParams()

    useEffect(() => {
        const fetchData = async () => {
            const base = `${BASE_URL}oncologist`
            const url = `${base}?npi=${id}`
            try {
                const response = await fetch(url)
                const data = await response.json()

                setData(data)
                setAddress(
                    data.address + ', ' + data.city + ', ' + data.zip_code
                )

                setTrialIDs(data.trial_id)
                setGeoIDs(data.geo_id)
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [id])

    useEffect(() => {
        const fetchDataTrial = async () => {
            setLoading(true)
            const base = `${BASE_URL}trial`
            try {
                const fetchedData = []
                for (const trial_id of trialIDs) {
                    const url = `${base}?trial_id=${trial_id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()

                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setTrialList(fetchedData)
            } catch (error) {
                console.log(error)
            }
            setLoading(false)
        }
        fetchDataTrial()
    }, [trialIDs])

    useEffect(() => {
        const fetchDataCounty = async () => {
            setLoading2(true)
            const base = `${BASE_URL}census_tract`
            try {
                const fetchedData = []
                for (const id of geoIDs) {
                    const county_id = id.slice(6)

                    const url = `${base}?geo_id=geoID/${county_id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()
                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setCounties(fetchedData)
            } catch (error) {
                console.log(error)
            }

            setLoading2(false)
        }
        fetchDataCounty()
    }, [geoIDs])

    if (!data) return <Loading />

    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <OncologistInst id={id} />
                <Typography
                    justifyContent={'center'}
                    fontFamily={'Serif'}
                    variant="h5"
                >
                    <p>Location of the Facility</p>
                </Typography>

                <Stack
                    width="80%"
                    height="400px"
                    sx={{
                        borderRadius: '10px',
                        mt: '10px',
                        mb: '10px',
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        border: '1px solid black',
                        backgroundColor: '#74b9ff',
                    }}
                >
                    <Map address={encodeURIComponent(address)}></Map>
                </Stack>
                <Stack width="100%">
                    <Typography
                        fontFamily={'Serif'}
                        variant="h5"
                        textAlign={'center'}
                    >
                        <p>Find clinical trials in this area: </p>
                    </Typography>
                    {!loading && (
                        <Grid container direction="row">
                            {trialList.map((trial, id) => (
                                <Grid
                                    item
                                    xs={12}
                                    sm={10}
                                    md={3}
                                    xl={3}
                                    key={trial.trial_id}
                                >
                                    <ClinicalTrialCard trial={trial} />
                                </Grid>
                            ))}
                        </Grid>
                    )}
                </Stack>
                <Stack width="100%">
                    <Typography
                        textAlign={'center'}
                        variant="h5"
                        fontFamily={'Serif'}
                    >
                        <p>Explore cities in this area: </p>
                    </Typography>
                    {!loading2 && (
                        <Grid container direction="row">
                            {counties.map((county) => (
                                <Grid
                                    item
                                    xs={12}
                                    sm={10}
                                    md={3}
                                    xl={3}
                                    key={county.geo_id}
                                >
                                    <CensusTractCard county={county} />
                                </Grid>
                            ))}
                        </Grid>
                    )}
                </Stack>
            </Container>
        </Box>
    )
}

export default OncologistDetail
