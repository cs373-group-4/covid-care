import { useState } from 'react'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Box, Tabs, Tab, Typography } from '@mui/material'

import OncologistGrid from './Oncologists'
import CensusTracts from './CensusTracts'
import ClinicalTrials from './ClinicalTrials'

function CustomTabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

CustomTabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    }
}

function SearchTabs(props) {
    const { searchTerm, value } = props
    return (
        <>
            <CustomTabPanel value={value} index={0}>
                <OncologistGrid onSearchPage searchInput={searchTerm} />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
                <ClinicalTrials onSearchPage searchInput={searchTerm} />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={2}>
                <CensusTracts onSearchPage searchInput={searchTerm} />
            </CustomTabPanel>
        </>
    )
}

function SearchPage() {
    const { term } = useParams()
    const [value, setValue] = useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    return (
        <Box sx={{ padding: '2rem' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="auto"
                    allowScrollButtonsMobile
                    aria-label="basic tabs example"
                >
                    <Tab label="Oncologists" {...a11yProps(0)} />
                    <Tab label="Clinical Trials" {...a11yProps(1)} />
                    <Tab label="Census Tracts" {...a11yProps(2)} />
                </Tabs>
                <SearchTabs key={term} searchTerm={term} value={value} />
            </Box>
        </Box>
    )
}

export default SearchPage
