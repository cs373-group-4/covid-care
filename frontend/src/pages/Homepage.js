import { Outlet } from 'react-router-dom'
import { Stack } from '@mui/material'

import Footer from '../layouts/Footer.js'
import Header from '../layouts/Header.js'
import Splash from '../layouts/Splash'

function Homepage() {
    return (
        <Stack data-testid="stack-component" sx={{ minHeight: '100vh' }}>
            <Header />

            <Outlet />

            <Splash />

            <Footer />
        </Stack>
    )
}

export default Homepage
