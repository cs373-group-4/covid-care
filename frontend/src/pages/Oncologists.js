import { useEffect, useState } from 'react'
import { Box } from '@mui/system'
import { Grid, Typography, Stack } from '@mui/material'
import { BASE_URL } from '../utils/config.js'

import OncologistCard from '../components/OncologistCard'
import PagintionComponent from '../components/PaginationController'
import SearchModule from '../components/SearchModule'
import { sort, order, filter } from '../utils/sortAndFilter.js'
import {
    CityFilter,
    CountyFilter,
    GenderFilter,
    OrderDropdown,
    SortDropdown,
} from '../components/FilterSortComponents.js'
import Loading from '../components/Loading.js'

const OncologistGrid = (props) => {
    const { onSearchPage, searchInput } = props
    const [page, setPage] = useState(1)
    const [fetchData, setFetchData] = useState([])
    const [size, setSize] = useState(0)
    // search input is the input from the search page for all models
    const [searchQuery, setSearchQuery] = useState(searchInput || '')
    const module = 'oncologist'
    const [url, setUrl] = useState(`${BASE_URL}oncologist_all`)

    const [totalPages, setTotalPages] = useState(1)
    const [dataList, setDataList] = useState([])
    const [totalResults, setTotalResults] = useState(8)

    useEffect(() => {
        if (searchQuery !== '') {
            if (searchQuery.toLowerCase() === 'female') {
                setUrl(`${BASE_URL}search?model=oncologist&search_val=f`)
            } else if (searchQuery.toLowerCase() === 'male') {
                setUrl(`${BASE_URL}search?model=oncologist&search_val=m`)
            } else {
                setUrl(
                    `${BASE_URL}search?model=oncologist&search_val=${searchQuery}`
                )
            }
        } else {
            setUrl(`${BASE_URL}oncologist_all`)
        }
    }, [searchQuery])

    // sorting
    const handleSort = (event) => {
        let newUrl = ''
        if (url.includes('search')) {
            setSearchQuery('')
            newUrl = `${BASE_URL}oncologist_all`
            setUrl(sort(event, newUrl))
        } else {
            console.log('handleSort', url)
            setUrl(sort(event, url))
        }
    }

    // ordering
    const handleOrder = (event) => {
        setUrl(order(event, url))
    }

    // filtering
    const handleFilter = (event) => {
        let newUrl = ''
        if (url.includes('search')) {
            setSearchQuery('')
            newUrl = `${BASE_URL}oncologist_all`
            setUrl(filter(event, newUrl))
        } else {
            console.log('handlefilter', url)
            setUrl(filter(event, url))
        }
        setUrl(filter(event, url))
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                console.log('ocologist url: ', url)
                const response = await fetch(url)
                const data = await response.json()
                setFetchData(data)
                setSize(data.length)
                setTotalPages(Math.ceil(size / 8))
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [searchQuery, url, size, page])

    // pagination
    useEffect(() => {
        var start = (page - 1) * 8
        var end = start + 8
        setDataList(fetchData.slice(start, end))
        const resultSoFar = page * 8
        setTotalResults(Math.min(resultSoFar, size))
        console.log(`searchQuery: ${searchQuery} |||| page: ${page}`)
    }, [page, fetchData, size, searchQuery])

    if (!dataList) return <Loading />

    return (
        <Box sx={{ padding: '2rem' }}>
            {!onSearchPage && (
                <>
                    <Typography
                        className="page-title"
                        marginBottom={'2rem'}
                        textAlign={'center'}
                        variant="h4"
                        fontFamily={'Serif'}
                    >
                        Oncologists
                    </Typography>
                    <Stack
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Box width={{ xs: '90%', sm: '40%' }} mb="1rem">
                            <SearchModule
                                module={module}
                                searchInput={setSearchQuery}
                            />
                        </Box>
                    </Stack>
                    <Stack
                        display="flex"
                        sx={{
                            flexDirection: {
                                xs: 'column',
                                md: 'row',
                            },
                        }}
                        justifyContent="center"
                        alignItems="center"
                    >
                        <SortDropdown
                            handleSort={handleSort}
                            menuItems={[
                                { value: 'name', label: 'Name' },
                                { value: 'clinic_name', label: 'Clinic' },
                            ]}
                        />
                        <OrderDropdown handleOrder={handleOrder} />
                        <GenderFilter handleFilter={handleFilter} />
                        <CityFilter handleFilter={handleFilter} />
                        <CountyFilter handleFilter={handleFilter} />
                    </Stack>
                </>
            )}
            <Grid container direction="row">
                {dataList.map((oncologist) => (
                    <Grid item xs={12} sm={10} md={3} key={oncologist.npi}>
                        <OncologistCard
                            oncologist={oncologist}
                            query={searchQuery}
                        />
                    </Grid>
                ))}
            </Grid>
            <Typography marginBottom={'2rem'} textAlign={'right'} variant="h6">
                Total results: {totalResults}/{size}
            </Typography>
            <PagintionComponent PageCount={totalPages} changePage={setPage} />
        </Box>
    )
}

export default OncologistGrid
