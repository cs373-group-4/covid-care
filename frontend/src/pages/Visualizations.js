import React from 'react'
import { Box, Container, Typography } from '@mui/material'

import CountyPopulation from '../components/D3/CountyPopulation'
import OncologistBarChart from '../components/D3/OncologistBarChart'
import TrialChart from '../components/D3/TrialChart'

function Visualizations() {
    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                    fontSize: '2rem',
                    marginTop: '2rem',
                }}
            >
                <Typography variant="h4" fontFamily={'Serif'}>
                    Visualizations
                </Typography>
                <Typography variant="h5" fontFamily="Serif" marginTop={10}>
                    Population in each County
                </Typography>
                <CountyPopulation />
                <Typography variant="h5" fontFamily="Serif" marginBottom={3}>
                    Number of Clinical Trials in each City
                </Typography>
                <TrialChart />
                <Typography variant="h5" fontFamily="Serif" marginTop={10}>
                    Oncologist Genders in each City
                </Typography>
                <OncologistBarChart />
                <Typography marginBottom={5}></Typography>
            </Container>
        </Box>
    )
}

export default Visualizations
