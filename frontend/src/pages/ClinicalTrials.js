import { useState, useEffect } from 'react'
import { Box } from '@mui/system'
import { Grid, Typography, Stack } from '@mui/material'
import { BASE_URL } from '../utils/config.js'

import ClinicalTrialCard from '../components/ClinicalTrialCard'
import PagintionComponent from '../components/PaginationController'
import SearchModule from '../components/SearchModule'
import { sort, order, filter } from '../utils/sortAndFilter.js'
import {
    CountyFilter,
    CityFilter,
    OrderDropdown,
    RecruitingStatusFilter,
    SortDropdown,
} from '../components/FilterSortComponents.js'
import Loading from '../components/Loading.js'

function ClinicalTrials(props) {
    const { onSearchPage, searchInput } = props
    const [page, setPage] = useState(1)
    const [fetchData, setFetchData] = useState([])
    const [size, setSize] = useState(0)
    const [url, setUrl] = useState(`${BASE_URL}trial_all`)

    const module = 'clinical_trial'
    const [totalPages, setTotalPages] = useState(1)
    const [dataList, setDataList] = useState([])
    const [searchQuery, setSearchQuery] = useState(searchInput || '')
    const [totalResults, setTotalResults] = useState(8)

    useEffect(() => {
        if (searchQuery !== '') {
            if (searchQuery.toLowerCase() === 'female') {
                setUrl(`${BASE_URL}search?model=trial&search_val=f`)
            } else if (searchQuery.toLowerCase() === 'male') {
                setUrl(`${BASE_URL}search?model=trial&search_val=m`)
            } else {
                setUrl(
                    `${BASE_URL}search?model=trial&search_val=${searchQuery}`
                )
            }
        } else {
            setUrl(`${BASE_URL}trial_all`)
        }
    }, [searchQuery])

    // sorting
    const handleSort = (event) => {
        let newUrl = ''
        if (url.includes('search')) {
            setSearchQuery('')
            newUrl = `${BASE_URL}trial_all`
            setUrl(sort(event, newUrl))
        } else {
            console.log('handleSort', url)
            setUrl(sort(event, url))
        }
    }

    // ordering
    const handleOrder = (event) => {
        setUrl(order(event, url))
    }

    // filtering
    const handleFilter = (event) => {
        let newUrl = ''
        if (url.includes('search')) {
            setSearchQuery('')
            newUrl = `${BASE_URL}trial_all`
            setUrl(filter(event, newUrl))
        } else {
            console.log('handlefilter', url)
            setUrl(filter(event, url))
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                console.log('clinical', url)
                const response = await fetch(url)
                const data = await response.json()

                setFetchData(data)
                setSize(data.length)
                setTotalPages(Math.ceil(size / 8))

                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [searchQuery, url, size, page])

    useEffect(() => {
        const start = (page - 1) * 8
        const end = start + 8
        setDataList(fetchData.slice(start, end))
        const resultSoFar = page * 8
        setTotalResults(Math.min(resultSoFar, size))
    }, [page, fetchData, size, searchQuery])

    if (!dataList) return <Loading />

    return (
        <Box sx={{ padding: '2rem' }}>
            {!onSearchPage && (
                <>
                    <Typography
                        className="page-title"
                        marginBottom={'2rem'}
                        textAlign={'center'}
                        variant="h4"
                        fontFamily={'Serif'}
                    >
                        Clinical Trials
                    </Typography>
                    <Stack
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Box width={{ xs: '90%', sm: '40%' }} mb="1rem">
                            <SearchModule
                                module={module}
                                searchInput={setSearchQuery}
                            />
                        </Box>
                    </Stack>
                    <Stack
                        display="flex"
                        sx={{
                            flexDirection: {
                                xs: 'column',
                                md: 'row',
                            },
                        }}
                        justifyContent="center"
                        alignItems="center"
                    >
                        <SortDropdown
                            handleSort={handleSort}
                            menuItems={[
                                { value: 'trial_id', label: 'Trial ID' },
                                { value: 'title', label: 'Title' },
                            ]}
                        />
                        <OrderDropdown handleOrder={handleOrder} />
                        <RecruitingStatusFilter handleFilter={handleFilter} />
                        <CityFilter handleFilter={handleFilter} />
                        <CountyFilter handleFilter={handleFilter} />
                    </Stack>
                </>
            )}
            <Grid container direction="row">
                {dataList.map((trial) => (
                    <Grid
                        item
                        xs={12}
                        sm={10}
                        md={3}
                        xl={3}
                        key={trial.trial_id}
                    >
                        <ClinicalTrialCard trial={trial} query={searchQuery} />
                    </Grid>
                ))}
            </Grid>
            <Typography marginBottom={'2rem'} textAlign={'right'} variant="h6">
                Total results: {totalResults}/{size}
            </Typography>
            <PagintionComponent PageCount={totalPages} changePage={setPage} />
        </Box>
    )
}

export default ClinicalTrials
