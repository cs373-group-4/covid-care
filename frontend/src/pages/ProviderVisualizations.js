import React from 'react'
import { Box, Container, Typography } from '@mui/material'

import ShelterRatingMap from '../components/D3/ShelterRatingMap'
import PharmacyCity from '../components/D3/PharmacyCity'
import PharmacyDistances from '../components/D3/PharmacyDistance'

function ProviderVisualizations() {
    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                    fontSize: '2rem',
                    marginTop: '2rem',
                    marginBottom: '2rem',
                    fontFamily: 'Serif',
                }}
            >
                <Typography variant="h4" fontFamily={'Serif'}>
                    Provider Visualizations
                </Typography>
                <Typography
                    variant="h5"
                    fontFamily="Serif"
                    marginTop={10}
                    marginBottom={3}
                >
                    Shelters Rating Line Chart
                </Typography>
                <ShelterRatingMap />
                <Typography
                    variant="h5"
                    fontFamily="Serif"
                    marginTop={10}
                    marginBottom={3}
                >
                    Number of Pharmacies in Each City
                </Typography>
                <PharmacyCity />
                <Typography
                    variant="h5"
                    fontFamily="Serif"
                    marginTop={10}
                    marginBottom={3}
                >
                    Pharmacies distance from Each City
                </Typography>
                <PharmacyDistances />
            </Container>
        </Box>
    )
}

export default ProviderVisualizations
