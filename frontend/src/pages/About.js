import { useEffect, useState } from 'react'
import { Container, Typography, Grid, Link, Button } from '@mui/material'
import { Box, styled } from '@mui/system'

import data from '../components/members.json'
import Member from '../layouts/Member'
import ToolsUsed from '../components/ToolsUsed'
import { fetchCommits, fetchIssues } from '../components/aboutUtils'

const SectionHeader = styled(Typography)({
    margin: '20px 0 10px 0',
})

export default function About() {
    const projectId = '50466775'
    const [commits, setCommits] = useState([])
    const [issues, setIssues] = useState([])

    useEffect(() => {
        const accessToken = 'glpat--bGAS5Z8Fr3HWenPwmmN'

        fetchCommits(projectId, accessToken)
            .then((allCommits) => {
                setCommits(allCommits)
            })
            .catch((error) => {
                console.error('Error:', error)
            })

        fetchIssues(projectId, accessToken)
            .then((allIssues) => {
                setIssues(allIssues)
            })
            .catch((error) => {
                console.error('Error:', error)
            })
    }, [])

    return (
        <Box
            sx={{
                height: '100%',
            }}
            data-testid="about-component"
        >
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    padding: '20px 24px',
                    gap: '20px',
                }}
            >
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        margin: '40px 0 20px 0',
                    }}
                >
                    <Typography className="page-title" variant="h4">
                        About Cancer Care
                    </Typography>
                </Box>
                <Box>
                    <SectionHeader variant="h5">
                        What is Cancer Care?
                    </SectionHeader>
                    <Typography>{data.description}</Typography>
                </Box>
                <Box>
                    <SectionHeader variant="h5">Group Members</SectionHeader>
                    <Grid container spacing={2}>
                        {data.members.map((d, i) => {
                            const memberCommits = commits.filter(
                                (c) => c?.author_email === d.email
                            )
                            const memberIssues = issues.filter(
                                (i) => i?.author?.username === d.gitlabId
                            )
                            return (
                                <Member
                                    key={i}
                                    member={d}
                                    commits={memberCommits}
                                    issues={memberIssues}
                                />
                            )
                        })}
                    </Grid>
                    <Typography sx={{ marginTop: '5px' }} variant="h6">
                        Total Commits: {commits.length}
                    </Typography>
                    <Typography variant="h6">
                        Total Issues: {issues.length}
                    </Typography>
                </Box>
                <Box>
                    <SectionHeader variant="h5">Tools</SectionHeader>
                    <ToolsUsed tools={data.tools} />
                </Box>
                <Box sx={{ paddingBottom: '20px' }}>
                    <SectionHeader variant="h5">APIs Scraped</SectionHeader>
                    {data.data.map((d) => (
                        <Typography>
                            <Link href={d.url}>{d.name}</Link>: {d.how}
                        </Typography>
                    ))}
                </Box>
                <Button
                    sx={{ alignSelf: 'center' }}
                    variant="outlined"
                    href={data.gitlabRepo}
                >
                    GitLab Repo
                </Button>
                <Button
                    sx={{ alignSelf: 'center' }}
                    variant="outlined"
                    href={data.postmanAPI}
                >
                    Postman API
                </Button>
            </Container>
        </Box>
    )
}
