import { Box, Typography, Stack } from '@mui/material'

import Footer from '../layouts/Footer.js'
import Header from '../layouts/Header.js'

export function ErrorPage() {
    return (
        <Box sx={{ padding: '2rem' }}>
            <Typography textAlign="center">Page not Found</Typography>
        </Box>
    )
}

export function ErrorHomepage() {
    return (
        <Stack data-testid="stack-component" sx={{ minHeight: '100vh' }}>
            <Header />
            <ErrorPage />
            <Box sx={{ flexGrow: 1 }} />
            <Footer />
        </Stack>
    )
}
