import { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Box } from '@mui/system'
import { Container, Typography, Stack, Grid } from '@mui/material'
import { BASE_URL } from '../utils/config.js'
import Map from '../components/Map'
import CountyCard from '../components/CountyCard'
import OncologistCard from '../components/OncologistCard'
import ClinicalTrialCard from '../components/ClinicalTrialCard'
import Loading from '../components/Loading.js'

function CountyDetail() {
    const [data, setData] = useState(null)

    const [address, setAddress] = useState('')
    const [loading, setLoading] = useState(false)
    const [oncologistIDs, setOncologistIDs] = useState(null)
    const [oncologists, setOncologists] = useState([])

    const [trialIDs, setTrialIDs] = useState(null)
    const [trialList, setTrialList] = useState([])
    const [loading1, setLoading1] = useState(false)

    const { id } = useParams()

    useEffect(() => {
        const fetchData = async () => {
            const base = `${BASE_URL}census_tract`
            const url = `${base}?geo_id=geoID/${id}`

            try {
                console.log(url)
                const response = await fetch(url)
                const data = await response.json()

                setData(data)
                setAddress(
                    data.county +
                        ', ' +
                        data.city +
                        ', ' +
                        data.name +
                        ', ' +
                        data.zip_code
                )
                setOncologistIDs(data.npi)
                setTrialIDs(data.trial_id)
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [id])

    useEffect(() => {
        const fetchDataOncologists = async () => {
            setLoading(true)
            const base = `${BASE_URL}oncologist`
            try {
                const fetchedData = []
                for (const id of oncologistIDs) {
                    const url = `${base}?npi=${id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()
                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setOncologists(fetchedData)
            } catch (error) {
                console.log(error)
            }
            setLoading(false)
        }
        fetchDataOncologists()
    }, [oncologistIDs])

    useEffect(() => {
        const fetchDataTrial = async () => {
            setLoading1(true)
            const base = `${BASE_URL}trial`
            try {
                const fetchedData = []
                for (const trial_id of trialIDs) {
                    const url = `${base}?trial_id=${trial_id}`
                    try {
                        const response = await fetch(url)
                        const data = await response.json()
                        fetchedData.push(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
                setTrialList(fetchedData)
            } catch (error) {
                console.log(error)
            }
            setLoading1(false)
        }
        fetchDataTrial()
    }, [trialIDs])

    if (!data) return <Loading />

    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                }}
            >
                <CountyCard county={data} />
                <Typography
                    justifyContent={'center'}
                    fontFamily={'Serif'}
                    variant="h5"
                >
                    <p>Location</p>
                </Typography>
                <Stack
                    width="80%"
                    height="400px"
                    sx={{
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        border: '1px solid black',
                        backgroundColor: '#74b9ff',
                    }}
                >
                    <Map address={encodeURIComponent(address)}></Map>
                </Stack>

                <Typography
                    justifyContent={'center'}
                    fontFamily={'Serif'}
                    variant="h5"
                >
                    <p>Oncologists in this area: </p>
                </Typography>
                {!loading && (
                    <Grid container direction="row">
                        {oncologists.map((oncologist) => (
                            <Grid
                                item
                                xs={12}
                                sm={10}
                                md={3}
                                xl={3}
                                key={oncologist.npi}
                            >
                                <OncologistCard oncologist={oncologist} />
                            </Grid>
                        ))}
                    </Grid>
                )}

                <Typography
                    justifyContent={'center'}
                    fontFamily={'Serif'}
                    variant="h5"
                >
                    <p>Clinical Trials in this area: </p>
                </Typography>
                {!loading1 && (
                    <Grid container direction="row">
                        {trialList.map((trial) => (
                            <Grid
                                item
                                xs={12}
                                sm={10}
                                md={3}
                                xl={3}
                                key={trial.trial_id}
                            >
                                <ClinicalTrialCard trial={trial} />
                            </Grid>
                        ))}
                    </Grid>
                )}
            </Container>
        </Box>
    )
}

export default CountyDetail
