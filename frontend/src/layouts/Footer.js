import React from 'react'
import Cookies from 'js-cookie'
import PropTypes from 'prop-types'
import { Link, Typography, Switch } from '@mui/material'
import { Box } from '@mui/system'

function Footer(props) {
    const { mode, setMode } = props

    const setCookieOnClick = () => {
        if (mode === 'dark') {
            Cookies.set('colorMode', 'light', { expires: 7 })
            setMode('light')
        } else {
            Cookies.set('colorMode', 'dark', { expires: 7 })
            setMode('dark')
        }
    }

    const isDarkMode = () => {
        const cookieValue = Cookies.get('colorMode')
        // if cookies is defined and set to dark
        return cookieValue && cookieValue === 'dark'
    }

    return (
        <Box
            sx={{
                backgroundColor: '#34495e',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            data-testid="footer-component"
        >
            <Typography variant="body2" color="white" p={1} mt={2}>
                {'Copyright © '}
                <Link color="inherit" href="http://localhost:3000/">
                    cancer-care
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <Switch
                    className="darkButton"
                    onChange={setCookieOnClick}
                    checked={isDarkMode()}
                />
                <Typography variant="body2" color="white">
                    Dark Mode
                </Typography>
            </Box>
        </Box>
    )
}

Footer.propTypes = {
    mode: PropTypes.string,
    setMode: PropTypes.func,
}

export default Footer
