import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import {
    IconButton,
    Menu,
    MenuItem,
    AppBar,
    Box,
    Toolbar,
    Button,
    InputBase,
} from '@mui/material'
import { styled, alpha } from '@mui/material/styles'
import SearchIcon from '@mui/icons-material/Search'
import MenuIcon from '@mui/icons-material/Menu'

import Logo from '../components/Logo'

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
    },
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}))

const pages = [
    'About',
    'Oncologists',
    'Clinical Trials',
    'Census Tracts',
    'Visualizations',
    'Provider Visualizations',
]

export default function Header() {
    const navigate = useNavigate()
    const [anchorEl, setAnchorEl] = useState(null)

    const handleMenuOpen = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleMenuClose = () => {
        setAnchorEl(null)
    }

    const navigateToPage = (page) => {
        navigate(`/pages/${page}`)
    }

    const handleSearchKeyDown = (event) => {
        if (event.target.value) {
            // only go to search page if there is input
            if (event.key === 'Enter' || event.keyCode === 13) {
                navigateToPage(`search/${event.target.value}`)
            }
        }
    }

    return (
        <Box data-testid="header-component">
            <AppBar
                sx={{ flexGrow: 1, backgroundColor: '#34495e' }}
                position="static"
            >
                <Toolbar>
                    <Logo
                        sx={{
                            display: 'flex',
                            width: '10rem',
                            '@media (max-width: 400px)': {
                                width: '8rem',
                            },
                        }}
                    />
                    {/* Mobile Menu */}
                    <Box sx={{ display: { xs: 'block', md: 'none' } }}>
                        <IconButton
                            color="inherit"
                            aria-controls="menu"
                            aria-haspopup="true"
                            onClick={handleMenuOpen}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu"
                            anchorEl={anchorEl}
                            open={Boolean(anchorEl)}
                            onClose={handleMenuClose}
                        >
                            {pages.map((page) => {
                                const newName = page.replace(/\s/g, '')
                                return (
                                    <MenuItem
                                        key={newName}
                                        id={`${newName}-mobile-menu`}
                                        onClick={() => {
                                            navigateToPage(newName)
                                            handleMenuClose()
                                        }}
                                    >
                                        {page}
                                    </MenuItem>
                                )
                            })}
                        </Menu>
                    </Box>

                    {/* Desktop Menu */}
                    <Box
                        sx={{
                            flexGrow: 1,
                            display: { xs: 'none', md: 'flex' },
                        }}
                    >
                        {pages.map((page) => {
                            const newName = page.replace(/\s/g, '')
                            return (
                                <Button
                                    key={page}
                                    id={`${newName}-menu`}
                                    onClick={() => {
                                        navigateToPage(newName)
                                    }}
                                    sx={{
                                        my: 2,
                                        color: 'white',
                                        display: 'block',
                                    }}
                                >
                                    {page}
                                </Button>
                            )
                        })}
                    </Box>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            onKeyDown={handleSearchKeyDown}
                            placeholder="Search…"
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </Search>
                </Toolbar>
            </AppBar>
        </Box>
    )
}
