import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Typography, Button } from '@mui/material'
import { Box } from '@mui/system'
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt'

import image from '../components/cancerbg1.png'

function Splash() {
    const navigate = useNavigate()
    const backgroundImage = `url(${image})`
    return (
        <Box
            sx={{
                backgroundImage: backgroundImage,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                display: 'flex',
                width: '100%',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                height: '90vh',
                position: 'relative',
            }}
        >
            <Box
                sx={{
                    position: 'absolute',
                    left: '10px',
                    top: '10px',
                    maxWidth: '300px',
                    textAlign: 'center', // Align the quote text to the right
                }}
            >
                <Typography variant="h5" color="white">
                    "Yesterday is gone, tomorrow has not yet come. We have only
                    today, let us begin."
                    <br />- Mother Teresa
                </Typography>
            </Box>
            <Box
                sx={{
                    position: 'absolute',
                    right: '20px',
                    bottom: '100px',
                    maxWidth: '300px',
                    display: 'flex',
                    flexDirection: 'column',
                    padding: '10px',
                    spacing: '2rem',
                    textAlign: 'center', // Align the quote text to the right
                }}
            >
                <Button
                    id="oncologist-quick"
                    onClick={() => navigate('/pages/Oncologists')}
                    sx={{
                        backgroundColor: '#747d8c',
                        '&:hover': {
                            backgroundColor: '#747d8c',
                            color: 'white',
                        },
                        margin: '10px',
                        color: 'black',
                        fontSize: '1.25rem',
                    }}
                    size="small"
                >
                    Oncologists <ArrowRightAltIcon />
                </Button>
                <Button
                    id="tracts-quick"
                    onClick={() => navigate('/pages/CensusTracts')}
                    sx={{
                        backgroundColor: '#747d8c',
                        '&:hover': {
                            backgroundColor: '#747d8c',
                            color: 'white',
                        },
                        margin: '10px',
                        color: 'black',
                        fontSize: '1.25rem',
                    }}
                    size="small"
                >
                    Census Tracts <ArrowRightAltIcon />
                </Button>
                <Button
                    id="trials-quick"
                    onClick={() => navigate('/pages/ClinicalTrials')}
                    sx={{
                        backgroundColor: '#747d8c',
                        '&:hover': {
                            backgroundColor: '#747d8c',
                            color: 'white',
                        },
                        margin: '10px',
                        color: 'black',
                        fontSize: '1.25rem',
                    }}
                    size="small"
                >
                    Clinical Trials <ArrowRightAltIcon />
                </Button>
            </Box>
            <Box
                sx={{
                    position: 'absolute',
                    right: '0',
                    bottom: '0',
                    paddingTop: '2px',
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    backgroundColor: '#cf6a87',
                    textAlign: 'center', // Align the quote text to the right
                }}
            >
                <Typography color="white">
                    <b style={{ fontSize: '1.1rem' }}>URGENT NEED PROGRAM</b>
                    <p>
                        Contact Us:1-800-4-CANCER |
                        FinancialAssistance@utexas.edu
                    </p>
                </Typography>
            </Box>
        </Box>
    )
}

export default Splash
