import { Outlet } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Box, Stack } from '@mui/material'

import Footer from './Footer'
import Header from './Header'

function MainLayout(props) {
    const { mode, setMode } = props
    return (
        <Stack data-testid="stack-component" sx={{ minHeight: '100vh' }}>
            <Header />

            <Outlet />

            <Box sx={{ flexGrow: 1 }} />

            <Footer mode={mode} setMode={setMode} />
        </Stack>
    )
}

MainLayout.propTypes = {
    mode: PropTypes.string,
    setMode: PropTypes.func,
}

export default MainLayout
