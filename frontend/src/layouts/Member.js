import { Grid, Typography, styled } from '@mui/material'

import { Item } from '../components/Item'

const MemberPhoto = styled('img')({
    padding: '10px 0px',
})

const AboutTypography = styled(Typography)({
    overflowWrap: 'break-word',
})

export default function Member(props) {
    const { member, commits, issues } = props
    const photo = member.photo
        ? require('../components/member_photos/' + member.photo)
        : ''

    return (
        <Grid item key={member.gitlabId} xs={6} sm={5} md={4}>
            <Item sx={{ display: 'flex', flexDirection: 'column' }}>
                <AboutTypography>{member.name}</AboutTypography>
                <MemberPhoto src={photo} />
                <AboutTypography>{member.gitlabId}</AboutTypography>
                <AboutTypography>{member.email}</AboutTypography>
                <AboutTypography>[{member.team}]</AboutTypography>
                <AboutTypography sx={{ padding: '10px 0px' }}>
                    {member.intro}
                </AboutTypography>
                {commits && (
                    <AboutTypography>{commits.length} commits</AboutTypography>
                )}
                {issues && (
                    <AboutTypography>{issues.length} issues</AboutTypography>
                )}
                <AboutTypography>{member.numTests} tests</AboutTypography>
            </Item>
        </Grid>
    )
}
