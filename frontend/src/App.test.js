// test Homepage component load successful
import '@testing-library/jest-dom'
import { render, screen, cleanup } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import Homepage from './pages/Homepage'
import Logo from './components/Logo'
import OncologistCard from './components/OncologistCard'
import CensusTractCard from './components/CensusTractCard'
import TableClinic from './components/TableClinic'
import ToolsUsed from './components/ToolsUsed'
import About from './pages/About'
import ClinicalTrial from './components/ClinicalTrial'

afterEach(() => {
    cleanup()
})

describe('Homepage tests', () => {
    it('Test 1: should render Logo component with img child', () => {
        render(
            <BrowserRouter>
                <Logo />
            </BrowserRouter>
        )

        const logoElement = screen.getByRole('img')
        expect(logoElement).toBeInTheDocument()
    })

    it('Test 2: should return RouterLink', () => {
        const disabledLink = true
        if (disabledLink) {
            render(
                <BrowserRouter>
                    <Logo />
                </BrowserRouter>
            )
        }
        const routerLink = screen.getByTestId('logo-1')
        expect(routerLink).toBeInTheDocument()
    })

    it('Test 3: should render the Homepage component', () => {
        render(
            <BrowserRouter>
                <Homepage />
            </BrowserRouter>
        )
        const stackElement = screen.getByTestId('stack-component')
        expect(stackElement).toBeInTheDocument()

        // check Header component loaded
        const headerElement = screen.getByTestId('header-component')
        expect(headerElement).toBeInTheDocument()

        // check Footer component loaded
        const footerElement = screen.getByTestId('footer-component')
        expect(footerElement).toBeInTheDocument()
    })
})

describe('OncologistCard tests', () => {
    it('Test 4: should render the OncologistCard component', () => {
        const oncologist = {
            address:
                '1453 E BERT KOUNS INDUSTRIAL LOOP CANCER TREATMENT CENTER',
            city: 'SHREVEPORT',
            clinic_name: 'CHRISTUS TRINITY CLINIC',
            county: 'Caddo Parish',
            gender: 'F',
            geo_id: [
                'geoId/2201794024',
                'geoId/2201794222',
                'geoId/2201794402',
                'geoId/2201794585',
                'geoId/2201794783',
                'geoId/2201794951',
                'geoId/2201795110',
                'geoId/2201795254',
                'geoId/2201795371',
                'geoId/2201795473',
                'geoId/2201795551',
                'geoId/2201795617',
            ],
            name: 'NEELIMA CHINTAPALLI',
            npi: '1013108141',
            phone_number: '3186814138',
            specialization: 'HEMATOLOGY/ONCOLOGY',
            trial_id: [21, 22, 23, 32, 37, 42, 46, 79, 85],
            zip_code: '71105',
        }

        render(
            <BrowserRouter>
                <OncologistCard oncologist={oncologist} />
            </BrowserRouter>
        )

        const cardElement = screen.getByTestId('oncologist-card-component')
        expect(cardElement).toBeInTheDocument()
    })

    it('Test 5: test moreInfoButton is click and page is navitage to /pages/Oncologists/1', () => {
        const oncologist = {
            address:
                '1453 E BERT KOUNS INDUSTRIAL LOOP CANCER TREATMENT CENTER',
            city: 'SHREVEPORT',
            clinic_name: 'CHRISTUS TRINITY CLINIC',
            county: 'Caddo Parish',
            gender: 'F',
            geo_id: [
                'geoId/2201794024',
                'geoId/2201794222',
                'geoId/2201794402',
                'geoId/2201794585',
                'geoId/2201794783',
                'geoId/2201794951',
                'geoId/2201795110',
                'geoId/2201795254',
                'geoId/2201795371',
                'geoId/2201795473',
                'geoId/2201795551',
                'geoId/2201795617',
            ],
            name: 'NEELIMA CHINTAPALLI',
            npi: '1013108141',
            phone_number: '3186814138',
            specialization: 'HEMATOLOGY/ONCOLOGY',
            trial_id: [21, 22, 23, 32, 37, 42, 46, 79, 85],
            zip_code: '71105',
        }
        render(
            <BrowserRouter>
                <OncologistCard oncologist={oncologist} />
            </BrowserRouter>
        )

        const moreInfoButtonElement = screen.getByTestId('moreInfoButton')
        expect(moreInfoButtonElement).toBeInTheDocument()

        moreInfoButtonElement.click()
        expect(window.location.pathname).toBe('/pages/Oncologists/1013108141')
    })
})

describe('CensusTractCard tests', () => {
    it(' Test 6: should render the CensusTractCard component', () => {
        const county = {
            city: 'Shreveport',
            county: 'caddo parish',
            geo_id: 'geoId/2201794024',
            name: 'District 1',
            npi: [
                '1013108141',
                '1275776718',
                '1285660316',
                '1306062625',
                '1386671584',
                '1457615395',
                '1528214798',
                '1528279965',
                '1588609549',
                '1699764589',
                '1811158587',
                '1982010583',
            ],
            population: 18402,
            trial_id: [21, 22, 23, 32, 37, 42, 46, 79, 85],
            zip_code: 71119,
        }
        render(
            <BrowserRouter>
                <CensusTractCard county={county} />
            </BrowserRouter>
        )

        const cardElement = screen.getByTestId('card-component')
        expect(cardElement).toBeInTheDocument()
    })

    it('Test 7: test moreInfoButton is click and page is navitage to /pages/CensusTracts/1', () => {
        const county = {
            city: 'Shreveport',
            county: 'caddo parish',
            geo_id: 'geoId/2201794024',
            name: 'District 1',
            npi: [
                '1013108141',
                '1275776718',
                '1285660316',
                '1306062625',
                '1386671584',
                '1457615395',
                '1528214798',
                '1528279965',
                '1588609549',
                '1699764589',
                '1811158587',
                '1982010583',
            ],
            population: 18402,
            trial_id: [21, 22, 23, 32, 37, 42, 46, 79, 85],
            zip_code: 71119,
        }

        render(
            <BrowserRouter>
                <CensusTractCard county={county} />
            </BrowserRouter>
        )

        const moreInfoButtonElement = screen.getByTestId('moreInfoButton')
        expect(moreInfoButtonElement).toBeInTheDocument()

        moreInfoButtonElement.click()
        expect(window.location.pathname).toBe('/pages/CensusTracts/2201794024')
    })
})

describe('Clinical trial tests', () => {
    it('Test 8: should render the Clinical trial component', () => {
        const trial = {
            address: '1501 Kings Highway',
            city: 'Shreveport',
            contact_email: 'LPost@lsuhsc.edu',
            contact_phone: '318-813-1404',
            county: 'Caddo Parish',
            geo_id: [
                'geoId/2201794024',
                'geoId/2201794222',
                'geoId/2201794402',
                'geoId/2201794585',
                'geoId/2201794783',
                'geoId/2201794951',
                'geoId/2201795110',
                'geoId/2201795254',
                'geoId/2201795371',
                'geoId/2201795473',
                'geoId/2201795551',
                'geoId/2201795617',
            ],
            npi: [
                '1013108141',
                '1275776718',
                '1285660316',
                '1306062625',
                '1386671584',
                '1457615395',
                '1528214798',
                '1528279965',
                '1588609549',
                '1699764589',
                '1811158587',
                '1982010583',
            ],
            purpose: 'Treatment',
            recruiting_status: 'CLOSED_TO_ACCRUAL',
            title: 'LSU Health Sciences Center at Shreveport - A Randomized Phase III Trial for Surgically Resected Early Stage Non-Small Cell Lung Cancer: Crizotinib versus Observation for Patients with Tumors Harboring the Anaplastic Lymphoma Kinase (ALK) Fusion Protein',
            trial_id: 21,
        }

        render(
            <BrowserRouter>
                <ClinicalTrial clinic={trial} />
            </BrowserRouter>
        )

        const cardComponent = screen.getByTestId('stack-component')
        expect(cardComponent).toBeInTheDocument()
    })

    it('Test 9: should render the TableClinic component', () => {
        const data = [
            {
                column1: 'column1a',
                column2: 'column2a',
            },
            {
                column1: 'column1b',
                column2: 'column2b',
            },
        ]

        render(<TableClinic data={data} />)

        const tableRowElement = screen.getAllByRole('row')
        expect(tableRowElement).toHaveLength(2)
    })
})

describe('About tests', () => {
    it('Test 10: should render the About component', () => {
        render(
            <BrowserRouter>
                <About />
            </BrowserRouter>
        )

        const mapComponent = screen.getByTestId('about-component')
        expect(mapComponent).toBeInTheDocument()
    })
})

describe('ToolsUsed tests', () => {
    it(' Test 11: should render the ToolsUsed component', () => {
        const tools = [
            {
                name: 'React',
                link: 'https://reactjs.org/',
                imgLink: 'https://reactjs.org/logo-180x180.png',
            },
            {
                name: 'Material-UI',
                link: 'https://material-ui.com/',
                imgLink: 'https://material-ui.com/static/logo_raw.svg',
            },
        ]

        render(
            <BrowserRouter>
                <ToolsUsed tools={tools} />
            </BrowserRouter>
        )

        const toolsUsedElement = screen.getByTestId('tools-used-component')
        expect(toolsUsedElement).toBeInTheDocument()
        const imgElements = screen.getAllByRole('img')
        expect(imgElements).toHaveLength(2)
    })
})
