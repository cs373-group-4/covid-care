/* Class for handling sorting and filtering URL additions */

// sorting
export const sort = (event, url) => {
    let frontendUrl = url.split('?')[0]
    console.log('FRONTEND: ' + frontendUrl)

    const sortVal = event.target.value
    console.log('sorting val: ' + sortVal)
    return `${frontendUrl}?sort=${sortVal}`
}

// ordering
export const order = (event, url) => {
    if (!url.includes('sort')) {
        // handles if order is set before sort
        return url
    }

    const orderVal = event.target.value
    console.log('order val:' + orderVal)

    // modify url
    console.log('old url: ' + url)
    let newUrl = url
    if (url.includes('&order=')) {
        newUrl = url.split('&order=')[0]
    }
    return newUrl.concat(`&order=${orderVal}`)
}

// filtering
export const filter = (event, url) => {
    let frontendUrl = url.split('?')[0]

    let f = event.target.value + ''
    const filterVal = f.split('-')
    console.log(
        'filter column: ' + filterVal[0] + ', filter value: ' + filterVal[1]
    )

    return `${frontendUrl}?filter=${filterVal[0]}&filter_value=${filterVal[1]}`
}
