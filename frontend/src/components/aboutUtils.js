import axios from 'axios'

export async function fetchCommits(
    projectId,
    accessToken,
    page = 1,
    commits = []
) {
    try {
        const response = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/repository/commits?ref_name=main&per_page=100&page=${page}`,
            { headers: { 'PRIVATE-TOKEN': accessToken } }
        )

        const newCommits = response.data

        if (newCommits.length === 0) {
            return commits
        }

        commits = commits.concat(newCommits)

        return fetchCommits(projectId, accessToken, page + 1, commits)
    } catch (error) {
        console.error('Error fetching commits:', error)
        throw error
    }
}

export async function fetchIssues(
    projectId,
    accessToken,
    page = 1,
    issues = []
) {
    try {
        const response = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/issues?scope=all&per_page=100&page=${page}`,
            { headers: { 'PRIVATE-TOKEN': accessToken } }
        )

        const newIssues = response.data

        if (newIssues.length === 0) {
            return issues
        }

        issues = issues.concat(newIssues)

        return fetchIssues(projectId, accessToken, page + 1, issues)
    } catch (error) {
        console.error('Error fetching issues:', error)
        throw error
    }
}
