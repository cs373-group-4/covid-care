import {
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@mui/material'
import { counties, cities, zipcodes } from '../utils/filterData'

// used for all 3 modules
export function SubmitFilterButton(props) {
    const { setReload } = props
    return (
        <Button
            size="medium"
            sx={{
                backgroundColor: '#576574',
                color: 'white',
                '&:hover': {
                    backgroundColor: '#95a5a6',
                },
            }}
            onClick={() => setReload(true)}
        >
            Submit
        </Button>
    )
}

export function SortDropdown(props) {
    const { menuItems, handleSort } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 80 }} size="small">
            <InputLabel id="sort">Sort</InputLabel>
            <Select
                labelId="sorter"
                id="sort-button"
                label="Sort"
                onChange={handleSort}
            >
                {menuItems.map((i) => (
                    <MenuItem value={i.value}>{i.label}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export function OrderDropdown(props) {
    const { handleOrder } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 90 }} size="small">
            <InputLabel id="order">Order</InputLabel>
            <Select
                labelId="orderer"
                id="order-button"
                label="Order"
                onChange={handleOrder}
            >
                <MenuItem value={'asc'}>Ascending</MenuItem>
                <MenuItem value={'desc'}>Descending</MenuItem>
            </Select>
        </FormControl>
    )
}

// used for census and clinical trials
export function CountyFilter(props) {
    const { handleFilter } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 95 }} size="small">
            <InputLabel id="filterByCounty">County</InputLabel>
            <Select
                labelId="filter-county-label"
                id="filter-county"
                label="County"
                onChange={handleFilter}
            >
                {counties.map((c) => (
                    <MenuItem value={`county-${c}`}>{c}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

// used for census and clinical trials
export function CityFilter(props) {
    const { handleFilter } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 75 }} size="small">
            <InputLabel id="sort-3">City</InputLabel>
            <Select
                labelId="sort-label-3"
                id="select-3"
                label="City"
                onChange={handleFilter}
            >
                {cities.map((c) => (
                    <MenuItem value={`city-${c}`}>{c}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export function ZipcodeFilter(props) {
    const { handleFilter } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 110 }} size="small">
            <InputLabel id="filterByZip">Zip Code</InputLabel>
            <Select
                labelId="filter-zip-label"
                id="filter-zip"
                label="Recruiting status"
                onChange={handleFilter}
            >
                {zipcodes.map((z) => (
                    <MenuItem value={`zip_code-${z}`}>{z}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export function RecruitingStatusFilter(props) {
    const { handleFilter } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 170 }} size="small">
            <InputLabel id="sort-2">Recruiting status</InputLabel>
            <Select
                labelId="sort-label-2"
                id="select-2"
                label="Recruiting status"
                onChange={handleFilter}
            >
                <MenuItem value={'recruiting_status-ACTIVE'}>Active</MenuItem>
                <MenuItem value={'recruiting_status-CLOSED_TO_ACCRUAL'}>
                    Closed to accrual
                </MenuItem>
                <MenuItem
                    value={'recruiting_status-TEMPORARILY_CLOSED_TO_ACCRUAL'}
                >
                    Temporarily closed to accrual
                </MenuItem>
            </Select>
        </FormControl>
    )
}

export function GenderFilter(props) {
    const { handleFilter } = props
    return (
        <FormControl sx={{ m: 1, minWidth: 110 }} size="small">
            <InputLabel id="filter-by-gender">Gender</InputLabel>
            <Select
                labelId="gender-filter"
                id="gender-filter-id"
                label="gender"
                onChange={handleFilter}
            >
                <MenuItem value={'gender-F'}>Female</MenuItem>
                <MenuItem value={'gender-M'}>Male</MenuItem>
            </Select>
        </FormControl>
    )
}
