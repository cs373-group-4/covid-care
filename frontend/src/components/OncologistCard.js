import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import {
    Card,
    CardMedia,
    CardActions,
    CardContent,
    Button,
    Typography,
} from '@mui/material'

import { highlightText } from './highlightText'
import { formatPhoneNumber } from '../utils/HelperFunctions.js'

// Template for displaying oncologist information
export default function OncologistCard({ oncologist, query }) {
    const navigate = useNavigate()

    const handleClick = () => {
        navigate(`/pages/Oncologists/${oncologist.npi}`)
    }

    const capitalizedOncologist = {
        name: highlightText(capitalize(oncologist.name), query),
        city: highlightText(capitalize(oncologist.city), query),
        clinic_name: highlightText(capitalize(oncologist.clinic_name), query),
        specialization: highlightText(
            capitalize(oncologist.specialization),
            query
        ),
        gender: highlightText(
            oncologist.gender === 'M' ? 'Male' : 'Female',
            query
        ),
        phoneNumber: highlightText(
            formatPhoneNumber(oncologist.phone_number),
            query
        ),
    }
    const [data, setData] = useState(capitalizedOncologist)

    useEffect(() => {
        setData(capitalizedOncologist)
    }, [oncologist, query])

    return (
        <div style={{ margin: '10px' }}>
            <Card
                sx={{ display: 'flex', flexDirection: 'column', maxWidth: 600 }}
                data-testid="oncologist-card-component"
            >
                <CardContent>
                    <CardMedia
                        sx={{ height: 300, mb: 1.5 }}
                        image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/oncologists/${oncologist.npi}.jpeg`}
                    />
                    <Typography variant="h5">{data.name}</Typography>
                    <Typography color="#2ecc71">{data.city}</Typography>
                    <Typography>Clinic: {data.clinic_name}</Typography>
                    <Typography>
                        Specialization: {data.specialization}
                    </Typography>
                    <Typography>Gender: {data.gender}</Typography>
                    <Typography>Phone number: {data.phoneNumber}</Typography>
                </CardContent>

                <CardActions>
                    <Button
                        onClick={handleClick}
                        size="medium"
                        data-testid="moreInfoButton"
                        sx={{
                            backgroundColor: '#576574',
                            color: 'white',
                            '&:hover': {
                                backgroundColor: '#95a5a6',
                            },
                        }}
                    >
                        More info
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

function capitalize(str) {
    let delimiter = ' '
    if (str.indexOf('/') !== -1) {
        delimiter = '/'
    }

    const strings = str.split(delimiter)
    let finalString =
        strings[0].charAt(0).toUpperCase() + strings[0].slice(1).toLowerCase()

    for (let i = 1; i < strings.length; i++) {
        finalString +=
            delimiter +
            strings[i].charAt(0).toUpperCase() +
            strings[i].slice(1).toLowerCase()
    }

    return finalString
}
