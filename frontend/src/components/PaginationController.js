import { useState, useEffect } from 'react'
import { Typography, Pagination, Stack } from '@mui/material'

export default function PaginationController({ PageCount, changePage }) {
    const [page, setPage] = useState(1)
    useEffect(() => {
        setPage(1)
    }, [PageCount])

    const handleChange = (event, value) => {
        setPage(value)
        changePage(value)
    }

    return (
        <Stack
            spacing={2}
            sx={{
                color: 'black',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: '30px',
            }}
        >
            <Typography>
                <Pagination
                    sx={{
                        '& .MuiPaginationItem-root': {
                            color: 'gray',
                        },
                        '& .MuiPaginationItem-root:hover': {
                            backgroundColor: 'gray',
                        },
                        '& .MuiPaginationItem-root:active': {
                            backgroundColor: 'gray',
                        },
                        '& .MuiPaginationItem-root.Mui-selected': {
                            backgroundColor: 'gray',
                            color: 'white',
                        },
                    }}
                    count={PageCount}
                    page={page}
                    onChange={handleChange}
                    showFirstButton
                    showLastButton
                />
            </Typography>
        </Stack>
    )
}
