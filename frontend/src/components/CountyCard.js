import React from 'react'
import { Stack, Typography, Card, CardMedia } from '@mui/material'

function CountyCard({ county }) {
    const countyId = county.geo_id.slice(6)
    return (
        <Stack
            width="80%"
            sx={{
                borderRadius: '10px',
                mt: '10px',
                mb: '10px',

                alignItems: 'center',
                display: 'flex',
                justifyContent: 'center',
                border: '1px solid black',
            }}
        >
            <Stack
                sx={{
                    mt: 2,
                    display: 'flex',
                    justifyContent: 'center',
                    alignContent: 'center',
                    flexDirection: 'column',
                }}
            >
                <Typography textAlign="center" fontFamily={'Serif'}>
                    <h2>{county.city}</h2>
                </Typography>

                <Stack
                    sx={{
                        display: 'block',
                        justifyContent: 'center',
                        alignContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Card
                        sx={{
                            display: 'block',
                            justifyContent: 'center',
                            width: { sx: '100px', md: '320' },
                        }}
                    >
                        <CardMedia
                            width="90%"
                            // width={{ sx: '80%', md: '100%' }}
                            height="100%"
                            component="img"
                            image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/census_tracts/${countyId}.jpeg`}
                            alt=""
                        />
                    </Card>
                </Stack>
                <Stack sx={{ ml: 4, display: 'flex', textAlign: 'center' }}>
                    <Typography variant="h7">
                        <p>City: {county.city}</p>
                        <p>District: {county.name}</p>
                        <p> Zip_code: {county.zip_code}</p>
                        <p>Population: {county.population}</p>
                        <p>Number of Oncologists: {county.npi.length} </p>
                        <p>
                            Number of clinical trials: {county.trial_id.length}
                        </p>
                    </Typography>
                </Stack>
            </Stack>
        </Stack>
    )
}

export default CountyCard
