import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import { highlightText } from './highlightText'
import Loading from './Loading'
import {
    Typography,
    Card,
    CardContent,
    CardMedia,
    CardActions,
    Button,
} from '@mui/material'

export default function ClinicalTrialCard({ trial, query }) {
    function getPlace() {
        return trial.title.slice(0, trial.title.indexOf('-')).trim()
    }
    function getTitle() {
        const extractedTitle = trial.title
            .slice(trial.title.indexOf('-') + 1, trial.title.indexOf(':'))
            .trim()

        const words = extractedTitle.split(' ')
        const truncatedTitle = words.slice(0, 5).join(' ') + '...'

        return truncatedTitle
    }

    const navigate = useNavigate()

    const handleClick = () => {
        navigate(`/pages/ClinicalTrials/${trial.trial_id}`)
    }
    const rawData = {
        title: highlightText(getTitle(trial.title), query),
        city: highlightText(trial.city, query),
        clinic: highlightText(getPlace(trial.title), query),
        county: highlightText(trial.county, query),
        status: highlightText(
            trial.recruiting_status === 'ACTIVE'
                ? 'Active'
                : 'Closed to accrual',
            query
        ),
        phone: highlightText(trial.contact_phone, query),
        email: highlightText(trial.contact_email || 'N/A', query),
    }

    const [data, setData] = useState({ rawData })

    useEffect(() => {
        setData(rawData)
        console.log(trial)
    }, [trial, query])

    if (!trial) {
        return <Loading />
    }

    return (
        <div style={{ margin: '10px' }} data-testid="card-component">
            <Card sx={{ maxWidth: 600 }}>
                <CardContent>
                    <CardMedia
                        sx={{ height: 300 }}
                        image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/clinical_trials/image${trial.trial_id}.jpg`}
                        title={data.title}
                    />
                    <Typography sx={{ mt: 1.5 }} variant="h6" gutterBottom>
                        {data.title}
                    </Typography>
                    <Typography color="#27ae60">{data.city}</Typography>
                    <Typography>Clinic: {data.clinic}</Typography>
                    <Typography>
                        {'Recruiting status: ' +
                            (trial.recruiting_status === 'ACTIVE'
                                ? 'Active'
                                : trial.recruiting_status ===
                                  'CLOSED_TO_ACCRUAL'
                                ? 'Closed to accrual'
                                : 'Temporarily closed to accrual')}
                    </Typography>
                    <Typography>
                        {'Phone number: ' + trial.contact_phone}
                    </Typography>
                    <Typography>{'Email: ' + trial.contact_email}</Typography>
                </CardContent>
                <CardActions>
                    <Button
                        onClick={handleClick}
                        size="medium"
                        sx={{
                            backgroundColor: '#576574',
                            color: 'white',
                            '&:hover': {
                                backgroundColor: '#95a5a6',
                            },
                        }}
                    >
                        More info
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}
