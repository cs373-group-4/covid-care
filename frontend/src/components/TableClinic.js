import '../App.css'

import { Table, TableBody, TableCell, TableRow } from '@mui/material'
const TableClinic = ({ data }) => {
    return (
        <Table style={{ width: { xs: '100px', md: '100%' } }}>
            <TableBody style={{ width: { xs: '100px', md: '100%' } }}>
                {data.map((row, index) => (
                    <TableRow
                        key={index}
                        fontsize={{ xs: '0.2rem', md: '1rem' }}
                    >
                        <TableCell
                            style={{ width: { xs: '20%', md: '100%' } }}
                            fontsize={{ xs: '0.2rem', md: '1rem' }}
                        >
                            {row.column1}
                        </TableCell>
                        <TableCell fontsize={{ xs: '0.1rem', md: '1rem' }}>
                            {row.column2}
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}

export default TableClinic
