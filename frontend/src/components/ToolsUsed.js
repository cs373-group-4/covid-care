import { Grid, Link, styled } from '@mui/material'

import { Item } from './Item'

const MemberPhoto = styled('img')({
    padding: '10px 0px',
})

export default function ToolsUsed(props) {
    const { tools } = props

    return (
        <Grid container spacing={2} data-testid="tools-used-component">
            {tools.map((t) => (
                <Grid item xs={3} sm={2} md={2} key={t.link}>
                    <Item sx={{ display: 'flex', flexDirection: 'column' }}>
                        <MemberPhoto src={t.imgLink} />
                        <Link href={t.link}>{t.name}</Link>
                    </Item>
                </Grid>
            ))}
        </Grid>
    )
}
