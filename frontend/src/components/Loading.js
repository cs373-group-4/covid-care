import { Box } from '@mui/system'
import { Container } from '@mui/material'

function Loading() {
    return (
        <Box className="outerContainer">
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100%',
                }}
            >
                Loading...
            </Container>
        </Box>
    )
}

export default Loading
