import { Stack } from '@mui/material'
import { useJsApiLoader } from '@react-google-maps/api'

export default function Map({ address }) {
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: 'AIzaSyCfY1vGqH0QkdcN8vPeKPmB1Qe-yjbSrBQ',
    })

    if (!isLoaded) return 'Loading...'

    return (
        <Stack width="100%" height="100%" data-testid="map-component">
            <iframe
                width="100%"
                height="100%"
                frameborder="0"
                style={{ border: 0 }}
                referrerpolicy="no-referrer-when-downgrade"
                src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyCfY1vGqH0QkdcN8vPeKPmB1Qe-yjbSrBQ&q=${address}`}
                allowfullscreen
            ></iframe>
        </Stack>
    )
}
