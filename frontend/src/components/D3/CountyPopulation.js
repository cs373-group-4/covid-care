import { useEffect, useState } from 'react'
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from 'recharts'
import Box from '@mui/material/Box'
import { BASE_URL } from '../../utils/config'

const COLORS = ['#0088FE', '#00C49F', '#EAB543', '#82589F', '#fd79a8']

const CountyPopulation = () => {
    const [data, setData] = useState('')

    useEffect(() => {
        const fetchData = async () => {
            const url = `${BASE_URL}census_tract_all`

            try {
                const response = await fetch(url)
                const data = await response.json()

                const cityPopulationMap = {}

                data.forEach((item) => {
                    const { county, population } = item
                    const words = county.split(' ')
                    const capitalizedWords = words.map(
                        (word) => word.charAt(0).toUpperCase() + word.slice(1)
                    )
                    const capitalizedCounty = capitalizedWords.join(' ')

                    if (!cityPopulationMap[capitalizedCounty]) {
                        cityPopulationMap[capitalizedCounty] = population
                    } else {
                        cityPopulationMap[capitalizedCounty] += population
                    }
                })

                const formattedData = Object.entries(cityPopulationMap).map(
                    ([name, value]) => ({
                        name,
                        value,
                    })
                )

                setData(formattedData)
            } catch (error) {
                console.log(error)
            }
        }

        fetchData()
    }, [])

    if (data.length === 0) {
        return (
            <Box
                width="100%"
                height={400}
                display="flex"
                alignItems="center"
                justifyContent="center"
            >
                Loading...
            </Box>
        )
    }
    return (
        <ResponsiveContainer width="100%" height={400}>
            <PieChart width={600} height={600}>
                <Pie
                    dataKey="value"
                    isAnimationActive={false}
                    data={data}
                    cx="50%"
                    cy="50%"
                    outerRadius={100}
                    fontSize={14}
                    label={({ name, value }) => {
                        if (window.innerWidth > 600) {
                            return `${name}: ${value.toLocaleString()}`
                        } else {
                            return `${value.toLocaleString()}`
                        }
                    }}
                >
                    {data.map((entry, index) => (
                        <Cell
                            key={`cell-${index}`}
                            fill={COLORS[index % COLORS.length]}
                        />
                    ))}
                </Pie>

                <Tooltip contentStyle={{ fontSize: 'initial' }} />
            </PieChart>
        </ResponsiveContainer>
    )
}

export default CountyPopulation
