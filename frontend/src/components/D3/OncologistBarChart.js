import { useState, useEffect } from 'react'
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts'
import Box from '@mui/material/Box'
import { BASE_URL } from '../../utils/config'

const OncologistBarChart = () => {
    const [formatedData, setFormatedData] = useState('')

    function convertSentenceCase(str) {
        const finalSentence = str.replace(/(^\w{1})|(\s+\w{1})/g, (letter) =>
            letter.toUpperCase()
        )
        return finalSentence
    }

    useEffect(() => {
        const fetchData = async () => {
            const url = `${BASE_URL}oncologist_all`

            try {
                const response = await fetch(url)
                const data = await response.json()

                const groupedData = data.reduce((result, item) => {
                    const { city, gender } = item

                    if (!result[city]) {
                        result[city] = {
                            city: convertSentenceCase(city.toLowerCase()),
                            Female: 0,
                            Male: 0,
                        }
                    }

                    if (gender === 'F') {
                        result[city].Female++
                    } else if (gender === 'M') {
                        result[city].Male++
                    }

                    return result
                }, {})
                const dataObject = Object.values(groupedData)

                setFormatedData(dataObject)
            } catch (error) {
                console.log(error)
            }
        }

        fetchData()
    }, [])

    if (!formatedData) {
        return (
            <Box
                width="100%"
                height={400}
                display="flex"
                alignItems="center"
                justifyContent="center"
            >
                Loading...
            </Box>
        )
    }
    return (
        <ResponsiveContainer width="70%" height={500}>
            <BarChart
                width={400}
                height={300}
                data={formatedData}
                fontSize="0.8rem"
                margin={{
                    top: 20,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="city" />
                <YAxis />
                <Tooltip contentStyle={{ fontSize: 'initial' }} />
                <Legend lable={{ fontSize: 'initial' }} />
                <Bar
                    dataKey="Female"
                    stackId="a"
                    fill="#8884d8"
                    tick={{ fontSize: 12 }}
                />
                <Bar
                    dataKey="Male"
                    stackId="a"
                    fill="#82ca9d"
                    tick={{ fontSize: 12 }}
                />
            </BarChart>
        </ResponsiveContainer>
    )
}

export default OncologistBarChart
