import { useEffect, useState, useRef } from 'react'
import * as Plot from '@observablehq/plot'

function ProviderCityPopulations() {
    const [data2, setData2] = useState([])
    const ref2 = useRef()

    useEffect(() => {
        const url2 = `https://api.stormshelters.me/cities`
        const fetchData = async () => {
            try {
                const response = await fetch(url2)
                const data = await response.json()
                setData2(data['cities'])
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (data2 === undefined) return
        console.log(data2)

        // draw the visualization
        const plot2 = Plot.plot({
            marginLeft: 70,
            marginBottom: 80,
            x: {
                label: 'Cities',
                tickRotate: -30,
            },
            y: {
                grid: true,
                label: 'Population',
            },
            marks: [
                Plot.ruleY([0]),
                Plot.barY(data2, { x: 'name', y: 'pop', fill: 'green' }),
                Plot.tip(data2, Plot.pointerX({ x: 'name', y: 'pop' })),
            ],
        })
        ref2.current.appendChild(plot2)
        return () => plot2.remove()
    }, [data2])

    return <div ref={ref2} />
}

export default ProviderCityPopulations
