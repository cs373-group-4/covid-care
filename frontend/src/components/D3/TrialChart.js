import { useEffect, useState, useRef } from 'react'
import * as Plot from '@observablehq/plot'
import { BASE_URL } from '../../utils/config'

function CityPopulations() {
    const [data2, setData2] = useState([])
    const ref2 = useRef()

    useEffect(() => {
        const url2 = `${BASE_URL}trial_all`
        const fetchData = async () => {
            try {
                const response = await fetch(url2)
                const data = await response.json()
                setData2(data)
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (data2 === undefined) return

        data2['trial_number'] = 0
        const aggTrials = Object.fromEntries(
            data2.map((trial) => [trial.city, 0])
        )
        data2.forEach((trial) => (aggTrials[trial.city] += 1))
        data2.forEach((trial) => (trial.trial_number = aggTrials[trial.city]))
        console.log(data2)

        // draw the visualization
        const plot2 = Plot.plot({
            marginLeft: 70,
            marginBottom: 80,
            x: {
                label: 'Cities',
                tickRotate: -30,
            },
            y: {
                grid: true,
                label: 'Number of Clinical Trials',
            },
            marks: [
                Plot.ruleY([0]),
                Plot.barY(data2, { x: 'city', y: 1, fill: 'steelblue' }),
                Plot.tip(data2, Plot.pointer({ x: 'city', y: 'trial_number' })),
                Plot.axisX({ color: 'gray', stroke: 'gray' }),
                Plot.axisY({ color: 'gray', stroke: 'gray' }),
            ],
        })
        ref2.current.appendChild(plot2)
        return () => plot2.remove()
    }, [data2])

    return <div ref={ref2} />
}

export default CityPopulations
