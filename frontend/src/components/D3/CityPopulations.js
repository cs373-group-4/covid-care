import { useEffect, useState, useRef } from 'react'
import * as Plot from '@observablehq/plot'
import { BASE_URL } from '../../utils/config'
function CityPopulations() {
    const [data2, setData2] = useState([])
    const ref2 = useRef()

    useEffect(() => {
        const url2 = `${BASE_URL}census_tract_all`
        const fetchData = async () => {
            try {
                const response = await fetch(url2)
                const data = await response.json()
                setData2(data)
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (data2 === undefined) return

        data2['city_pop'] = 0
        const aggPopulations = Object.fromEntries(
            data2.map((tract) => [tract.city, 0])
        )
        data2.forEach(
            (tract) => (aggPopulations[tract.city] += tract.population)
        )
        data2.forEach((tract) => (tract.city_pop = aggPopulations[tract.city]))

        // draw the visualization
        const plot2 = Plot.plot({
            marginLeft: 70,
            marginBottom: 80,
            x: {
                label: 'Cities',
                tickRotate: -30,
            },
            y: {
                grid: true,
                label: 'Population',
            },
            marks: [
                Plot.ruleY([0]),
                Plot.barY(data2, {
                    x: 'city',
                    y: 'population',
                    fill: 'steelblue',
                    sort: 'city',
                }),
                Plot.tip(data2, Plot.pointer({ x: 'city', y: 'city_pop' })),
            ],
        })
        ref2.current.appendChild(plot2)
        return () => plot2.remove()
    }, [data2])

    return <div ref={ref2} />
}

export default CityPopulations
