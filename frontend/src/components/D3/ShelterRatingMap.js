import { useEffect, useState } from 'react'
import {
    LineChart,
    Line,
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts'
import Box from '@mui/material/Box'

const ShelterRatingMap = () => {
    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const url = `https://api.stormshelters.me/shelters?page=${1}&per_page=${36}`

            try {
                const response = await fetch(url)
                const data = await response.json()
                const extractedData = data.shelters.map((shelter) => {
                    const { name, rating } = shelter
                    return { name, rating }
                })
                setData(extractedData)
            } catch (error) {
                console.log(error)
            }
        }

        fetchData()
    }, [])

    if (data.length === 0) {
        return (
            <Box
                width="100%"
                height={500}
                display="flex"
                alignItems="center"
                justifyContent="center"
            >
                Loading...
            </Box>
        )
    }

    const xAxisTicks = data
        .filter((_, index) => index % 10 === 0)
        .map((entry) => entry.name) // Include every 10th name

    return (
        <ResponsiveContainer width="100%" height={500}>
            <LineChart
                width={500}
                height={300}
                data={data}
                fontSize="1.0rem"
                margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
            >
                <Line type="monotone" dataKey="rating" stroke="#0984e3" />
                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                <XAxis dataKey="name" ticks={xAxisTicks} />
                <Legend />
                <YAxis />
                <Tooltip contentStyle={{ fontSize: 'initial' }} />
            </LineChart>
        </ResponsiveContainer>
    )
}

export default ShelterRatingMap
