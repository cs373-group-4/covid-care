import { useEffect, useState, useRef } from 'react'
import * as Plot from '@observablehq/plot'

function PharmacyCity() {
    const [data2, setData2] = useState([])
    const ref2 = useRef()

    useEffect(() => {
        const url2 = `https://api.stormshelters.me/pharmacies?per_page=124`
        const fetchData = async () => {
            try {
                const response = await fetch(url2)
                const data = await response.json()
                setData2(data['pharmacies'])
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (data2 === undefined) return
        console.log(data2)

        data2['pharm_num'] = 0
        const aggPharmacies = Object.fromEntries(
            data2.map((pharm) => [pharm.city, 0])
        )
        data2.forEach((pharm) => (aggPharmacies[pharm.city] += 1))
        data2.forEach((pharm) => (pharm.pharm_num = aggPharmacies[pharm.city]))

        // draw the visualization
        const plot2 = Plot.plot({
            marginLeft: 70,
            marginBottom: 80,
            x: {
                label: 'Cities',
            },
            y: {
                grid: true,
                label: 'Pharmacies',
            },
            marks: [
                Plot.ruleY([0]),
                Plot.barY(data2, { x: 'city', y: 1, fill: 'steelblue' }),
                Plot.tip(data2, Plot.pointerX({ x: 'city', y: 'pharm_num' })),
                Plot.axisX({ color: 'gray', stroke: 'gray', tickRotate: 90 }),
                Plot.axisY({ color: 'gray', stroke: 'gray' }),
            ],
        })
        ref2.current.appendChild(plot2)
        return () => plot2.remove()
    }, [data2])

    return <div ref={ref2} />
}

export default PharmacyCity
