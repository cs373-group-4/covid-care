import { useEffect, useState, useRef } from 'react'
import * as Plot from '@observablehq/plot'

function PharmacyDistances() {
    const [data2, setData2] = useState([])
    const ref2 = useRef()

    useEffect(() => {
        const url2 = `https://api.stormshelters.me/pharmacies?per_page=124`
        const fetchData = async () => {
            try {
                const response = await fetch(url2)
                const data = await response.json()
                setData2(data['pharmacies'])
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (data2 === undefined) return

        // draw the visualization
        const plot2 = Plot.plot({
            marginLeft: 100,
            marginBottom: 40,
            y: {
                label: 'Cities',
            },
            x: {
                grid: true,
                label: 'Distance',
                ticks: 15,
            },
            color: { legend: true },
            marks: [
                Plot.dot(data2, {
                    y: 'city',
                    x: 'distance_m',
                    r: 5,
                    stroke: 'name',
                    channels: { name: 'name' },
                    tip: true,
                }),
                Plot.axisX({ color: 'gray', stroke: 'gray' }),
                Plot.axisY({ color: 'gray', stroke: 'gray' }),
            ],
        })
        ref2.current.appendChild(plot2)
        return () => plot2.remove()
    }, [data2])

    return <div ref={ref2} />
}

export default PharmacyDistances
