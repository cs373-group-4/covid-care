import { useState, useEffect } from 'react'
import { Typography, Card, CardMedia, Stack } from '@mui/material'
import Loading from './Loading'
import { formatPhoneNumber } from '../utils/HelperFunctions'

export default function OncologistInst({ id }) {
    const [oncologist, setOncologist] = useState([])
    const [address, setAddress] = useState('')
    const [facility, setFacility] = useState('')

    function convertSentenceCase(str) {
        const finalSentence = str.replace(/(^\w{1})|(\s+\w{1})/g, (letter) =>
            letter.toUpperCase()
        )
        return finalSentence
    }

    useEffect(() => {
        const fetchData = async () => {
            const base = `https://backend.cancer-care.me/oncologist`
            const url = `${base}?npi=${id}`

            try {
                console.log(url)
                const response = await fetch(url)
                const data = await response.json()

                setOncologist(data)
                const rawAddress =
                    data.address + ', ' + data.city + ', ' + data.zip_code
                setAddress(convertSentenceCase(rawAddress.toLowerCase()))

                const rawFacility = data.clinic_name
                setFacility(convertSentenceCase(rawFacility.toLowerCase()))
                return data
            } catch (error) {
                console.log(error)
            }
        }
        fetchData()
    }, [id])

    if (!oncologist) {
        return <Loading />
    }

    return (
        <Stack
            width="80%"
            sx={{
                borderRadius: '10px',
                mt: '10px',
                mb: '10px',
                display: 'flex',
                justifyContent: 'center',
            }}
        >
            <Stack
                flexWrap="wrap"
                justifyContent="flex-start"
                flexDirection="row"
                display="flex"
            >
                <Card sx={{ width: '180px', justifyContent: 'center' }}>
                    <CardMedia
                        width="100%"
                        height="100%"
                        component="img"
                        alt=""
                        image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/oncologists/${oncologist.npi}.jpeg`}
                    />
                </Card>
                <Stack ml={4}>
                    <Typography
                        sx={{ margin: '10' }}
                        fontFamily={'Serif'}
                        textAlign={'flex-start'}
                    >
                        <h2>{'DR. ' + oncologist.name}</h2>
                    </Typography>
                </Stack>
            </Stack>
            <Stack
                sx={{
                    mt: 2,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                }}
            >
                <Stack>
                    <Typography>
                        <b>Gender: </b>
                    </Typography>
                </Stack>
                <Stack sx={{ ml: 4, display: 'flex' }}>
                    <Typography>
                        {oncologist.gender === 'M' ? 'Male' : 'Female'}
                    </Typography>
                </Stack>
            </Stack>

            <Stack
                sx={{
                    mt: 2,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                }}
            >
                <Stack>
                    <Typography>
                        <b>Facility: </b>
                    </Typography>
                </Stack>
                <Stack sx={{ ml: 4, display: 'flex' }}>
                    <Typography>{facility}</Typography>
                </Stack>
            </Stack>
            <Stack
                sx={{
                    mt: 2,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                }}
            >
                <Stack>
                    <Typography>
                        <b>Address: </b>
                    </Typography>
                </Stack>
                <Stack sx={{ ml: 4, display: 'flex' }}>
                    <Typography>{address}</Typography>
                </Stack>
            </Stack>
            <Stack
                sx={{
                    mt: 2,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                }}
            >
                <Stack>
                    <Typography>
                        <b>Contact: </b>
                    </Typography>
                </Stack>
                <Stack sx={{ ml: 4, display: 'flex' }}>
                    <Typography>
                        {formatPhoneNumber(oncologist.phone_number)}
                    </Typography>
                </Stack>
            </Stack>
        </Stack>
    )
}
