import { useEffect, useState } from 'react'
import { Stack, Card, Typography, CardMedia } from '@mui/material'

import TableClinic from './TableClinic'

function ClinicalTrial({ clinic }) {
    const [address, setAddress] = useState('')
    const [place, setPlace] = useState('')
    const [description, setDescription] = useState('')
    const [title, setTitle] = useState('')

    function convertSentenceCase(str) {
        const finalSentence = str.replace(/(^\w{1})|(\s+\w{1})/g, (letter) =>
            letter.toUpperCase()
        )
        return finalSentence
    }

    useEffect(() => {
        setAddress(clinic.address + ', ' + clinic.city + ', ' + clinic.county)
        setPlace(
            convertSentenceCase(
                clinic.title.slice(0, clinic.title.indexOf('-')).toLowerCase()
            )
        )
        const title =
            clinic.title.indexOf(':') !== -1
                ? clinic.title
                      .slice(
                          clinic.title.indexOf('-') + 1,
                          clinic.title.indexOf(':')
                      )
                      .trim()
                : clinic.title.slice(clinic.title.indexOf('-') + 1).trim()

        setTitle(title)

        setDescription(
            clinic.title.substring(clinic.title.indexOf(':') + 1).trim()
        )
    }, [clinic])

    const data = [
        {
            column1: 'Recruitment status: ',
            column2:
                clinic.recruiting_status === 'ACTIVE'
                    ? 'Active'
                    : clinic.recruiting_status === 'CLOSED_TO_ACCRUAL'
                    ? 'Closed to accrual'
                    : 'Temporarily closed to accrual',
        },
        {
            column1: 'Purpose: ',
            column2: clinic.purpose,
        },
        {
            column1: 'Facility: ',
            column2: place,
        },
        { column1: 'Contact phone', column2: clinic.contact_phone },
        { column1: 'Contact email', column2: clinic.contact_email || 'N/A' },
        { column1: 'Address', column2: address },
    ]

    return (
        <Stack
            width={{ xs: '100%', md: '80%' }}
            sx={{
                borderRadius: '10px',
                mt: '10px',
                mb: '10px',

                display: 'flex',
                justifyContent: 'center',
            }}
            data-testid="stack-component"
        >
            <Stack>
                <Typography
                    sx={{ margin: '10' }}
                    fontFamily={'Serif'}
                    textAlign={'center'}
                >
                    <h2>Title: {title}</h2>
                </Typography>
            </Stack>
            <Stack
                flexWrap="wrap"
                justifyContent="center"
                flexDirection="row"
                display="flex"
                // ml="10px"
            >
                <Card sx={{ width: '50%', justifyContent: 'center' }}>
                    <CardMedia
                        width="100%"
                        height="100%"
                        component="img"
                        image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/clinical_trials/image${clinic.trial_id}.jpg`}
                        alt="image of a lab"
                    />
                </Card>
            </Stack>
            <Stack
                sx={{
                    mt: 4,

                    justifyContent: 'center',
                    flexDirection: 'row',
                }}
            >
                <Stack sx={{ ml: 1 }}>
                    <Typography>
                        <p>Description: {description}</p>
                    </Typography>
                </Stack>
            </Stack>
            <Stack sx={{ ml: { xs: 0, md: 3 }, display: 'flex' }}>
                <TableClinic data={data} />
            </Stack>
        </Stack>
    )
}

export default ClinicalTrial
