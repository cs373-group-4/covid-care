import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {
    Card,
    CardMedia,
    CardActions,
    CardContent,
    Button,
    Typography,
} from '@mui/material'

import { highlightText } from './highlightText'

// Template for displaying census tract information
export default function CensusTractCard({ county, query }) {
    const navigate = useNavigate()
    const handleClick = () => {
        const id = county.geo_id.split('/')[1]
        console.log('id', id)
        navigate(`/pages/CensusTracts/${id}`)
    }
    const rawData = {
        name: highlightText(county.name, query),
        city: highlightText(county.city, query),
        county: highlightText(capitalize(county.county), query),
        district: highlightText(county.name, query),
        population: highlightText(county.population.toString(), query),
        zip_code: highlightText(county.zip_code.toString(), query),
    }

    const [data, setData] = useState({ rawData })

    useEffect(() => {
        setData(rawData)
    }, [county, query])

    return (
        <div style={{ margin: '10px' }} data-testid="card-component">
            <Card sx={{ maxWidth: 600 }}>
                <CardContent>
                    <CardMedia
                        sx={{ height: 250 }}
                        image={`https://cancer-care-images.s3.us-east-2.amazonaws.com/census_tracts/${county.geo_id.replace(
                            'geoId/',
                            ''
                        )}.jpeg`}
                        title={data.county}
                    />
                    <Typography sx={{ mt: 1.5 }} variant="h5" gutterBottom>
                        {data.name}
                    </Typography>
                    <Typography color="#2ecc71">{data.city}</Typography>
                    <Typography>County: {data.county}</Typography>
                    <Typography>District: {data.name}</Typography>
                    <Typography>Population: {data.population}</Typography>
                    <Typography>Zip code: {data.zip_code}</Typography>
                </CardContent>
                <CardActions>
                    <Button
                        onClick={handleClick}
                        size="medium"
                        data-testid="moreInfoButton"
                        sx={{
                            backgroundColor: '#576574',
                            color: 'white',
                            '&:hover': {
                                backgroundColor: '#95a5a6',
                            },
                        }}
                    >
                        More info
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

function capitalize(str) {
    let delimiter = ' '
    if (str.indexOf('/') !== -1) {
        delimiter = '/'
    }

    const strings = str.split(delimiter)
    let finalString =
        strings[0].charAt(0).toUpperCase() + strings[0].slice(1).toLowerCase()

    for (let i = 1; i < strings.length; i++) {
        finalString +=
            delimiter +
            strings[i].charAt(0).toUpperCase() +
            strings[i].slice(1).toLowerCase()
    }

    return finalString
}
