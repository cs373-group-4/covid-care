import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

URL = 'https://www.cancer-care.me/'

class SeleniumTestsLoads(unittest.TestCase):
    # class method set up & tear down from https://gitlab.com/visiopi/re-park-able/-/blob/main/frontend/my-app/__gui_tests__/gui_Tests.py?ref_type=heads
    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--window-size=1920x1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
        self.driver.maximize_window()
        self.driver.get(URL)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()


    def test_webpage_should_load(self):
        self.assertEqual(self.driver.current_url, URL)

class SeleniumTestsURLs(unittest.TestCase):
    data = ["Oncologists", "ClinicalTrials", "CensusTracts"]

    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--window-size=1920x1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
        self.driver.maximize_window()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_urls(self):
        for d in self.data:
            self.driver.get(URL + '/pages/' + d)
            self.assertEqual(self.driver.current_url, URL + '/pages/' + d)
            
        

class SeleniumTestsButtons(unittest.TestCase):
    data = [
        {
            "testName": "Oncologist page load - quick button",
            "itemName": "oncologist-quick",
            "pageTitle": "Oncologists",
        },
        {
            "testName": "Oncologist page load - menu button",
            "itemName": "Oncologists-menu",
            "pageTitle": "Oncologists",
        },
        {
            "testName": "Trials page load - quick button",
            "itemName": "trials-quick",
            "pageTitle": "Clinical Trials",
        },
        {
            "testName": "Trial page load - menu button",
            "itemName": "ClinicalTrials-menu",
            "pageTitle": "Clinical Trials",
        },
        {
            "testName": "Tracts page load - quick button",
            "itemName": "tracts-quick",
            "pageTitle": "Census Tracts",
        },
        {
            "testName": "Tracts page load - menu button",
            "itemName": "CensusTracts-menu",
            "pageTitle": "Census Tracts",
        },
        {
            "testName": "About page load - menu button",
            "itemName": "About-menu",
            "pageTitle": "About Cancer Care",
        },
    ]

    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--window-size=1920x1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
        self.driver.maximize_window()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_buttons(self):
        for d in self.data:
            self.driver.get(URL)
            
            button = self.driver.find_element(By.ID, d["itemName"])
            button.click()

            title_element = self.driver.find_element(By.CLASS_NAME, "page-title")
            text = title_element.text
            self.assertEqual(d["pageTitle"], text)

# class SeleniumTestsDarkMode(unittest.TestCase):
#     @classmethod
#     def setUpClass(self):
#         options = webdriver.ChromeOptions()
#         options.add_experimental_option('excludeSwitches', ['enable-logging'])
#         options.add_argument("--headless")
#         options.add_argument("--window-size=1920x1080")
#         options.add_argument("--no-sandbox")
#         options.add_argument("--disable-dev-shm-usage")
#         chrome_prefs = {}
#         options.experimental_options["prefs"] = chrome_prefs
#         # Disable images
#         chrome_prefs["profile.default_content_settings"] = {"images": 2}

#         self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
#         self.driver.maximize_window()

#     @classmethod
#     def tearDownClass(self):
#         self.driver.quit()

#     def test_button(self):
#         self.driver.get(URL + '/pages/about')
#         about_page = self.driver.find_element(By.CLASS_NAME, "App")
#         background_color = about_page.value_of_css_property('background-color')
#         self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')
#         dark_switch = self.driver.find_element(By.CLASS_NAME, "darkButton")
#         dark_switch.click()
#         self.assertEqual(background_color, 'rgba(18, 18, 18, 1)')
        


# class SeleniumTestsInstances(unittest.TestCase):
#     @classmethod
#     def setUpClass(self):
#         options = webdriver.ChromeOptions()
#         options.add_experimental_option('excludeSwitches', ['enable-logging'])
#         options.add_argument("--headless")
#         options.add_argument("--window-size=1920x1080")
#         options.add_argument("--no-sandbox")
#         options.add_argument("--disable-dev-shm-usage")
#         chrome_prefs = {}
#         options.experimental_options["prefs"] = chrome_prefs
#         # Disable images
#         chrome_prefs["profile.default_content_settings"] = {"images": 2}

#         self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
#         self.driver.maximize_window()

#     @classmethod
#     def tearDownClass(self):
#         self.driver.quit()

#     def test_oncologists(self):
#         self.driver.get(URL + '/pages/Oncologists')
#         more_info_button = self.driver.find_element(By.CSS_SELECTOR, "[data-testid='moreInfoButton']")
#         more_info_button.click()
#         map = self.driver.find_element(By.CSS_SELECTOR, "[data-testid='map-component']")
#         self.assertEqual(map.is_displayed(), True)


if __name__ == "__main__":
    unittest.main()
