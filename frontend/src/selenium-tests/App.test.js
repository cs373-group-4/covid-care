const { Builder, By } = require('selenium-webdriver')
const { describe, it, beforeEach, afterEach } = require('mocha')
const assert = require('assert')
const chrome = require('selenium-webdriver/chrome')

const screen = {
    width: 900,
    height: 480,
}

describe('Selenium Tests - Loads', function () {
    this.timeout(10000)

    it('Webpage should load', async function () {
        const driver = await new Builder()
            .forBrowser('chrome')
            .setChromeOptions(
                new chrome.Options().headless().windowSize(screen)
            )
            .build()
        await driver.get('https://www.cancer-care.me/')
        const title = await driver.getTitle()
        assert.equal('Cancer Care', title)
        if (driver) await driver.quit()
    })
})

describe('Selenium Tests - Buttons', function () {
    this.timeout(10000)

    const data = [
        {
            testName: 'Oncologist page load - quick button',
            itemName: 'oncologist-quick',
            pageTitle: 'Oncologists',
        },
        {
            testName: 'Oncologist page load - menu button',
            itemName: 'Oncologists-menu',
            pageTitle: 'Oncologists',
        },
        {
            testName: 'Trials page load - quick button',
            itemName: 'trials-quick',
            pageTitle: 'Clinical Trials',
        },
        {
            testName: 'Trial page load - menu button',
            itemName: 'ClinicalTrials-menu',
            pageTitle: 'Clinical Trials',
        },
        {
            testName: 'Tracts page load - quick button',
            itemName: 'tracts-quick',
            pageTitle: 'Census Tracts',
        },
        {
            testName: 'Tracts page load - menu button',
            itemName: 'CensusTracts-menu',
            pageTitle: 'Census Tracts',
        },
        {
            testName: 'About page load - menu button',
            itemName: 'About-menu',
            pageTitle: 'About Cancer Care',
        },
    ]

    data.forEach((d) => {
        it(d.testName, async function () {
            const driver = await new Builder()
                .forBrowser('chrome')
                .setChromeOptions(
                    new chrome.Options().headless().windowSize(screen)
                )
                .build()
            await driver.get('https://www.cancer-care.me/')
            const button = await driver.findElement(By.id(d.itemName))
            await button.click()

            const titleObject = await driver.findElement(
                By.className('page-title')
            )
            const text = await titleObject.getText()
            assert.equal(d.pageTitle, text)
            if (driver) await driver.quit()
        })
    })
})

// describe('Selenium Tests - Pagination', function () {
//     this.timeout(10000)
//     let driver

//     const data = [
//         {
//             testName: 'Oncologists page - pagination',
//             urlName: 'Oncologists',
//         },
//         {
//             testName: 'Clinical Trials page - pagination',
//             urlName: 'ClinicalTrials',
//         },
//         {
//             testName: 'Census Tracts page - pagination',
//             urlName: 'CensusTracts',
//         },
//     ]

//     data.forEach((d) => {
//         it(d.testName, async function () {
//             driver = await new Builder()
//                 .forBrowser('chrome')
//                 .setChromeOptions(
//                     new chrome.Options().headless().windowSize(screen)
//                 )
//                 .build()
//             await driver.get(`https://www.cancer-care.me/pages/${d.urlName}`)

//             let pageCount = 3 // change later
//             let oldHtml = driver.getPageSource()
//             for (let i = 1; i <= pageCount; i++) {
//                 const button = driver.findElement(
//                     By.xpath(`//button[text()='${i}']`)
//                 )
//                 await button.click()

//                 // assert page changed
//                 let newHtml = driver.getPageSource()
//                 assert.notEqual(oldHtml, newHtml)

//                 // update old html
//                 oldHtml = newHtml
//             }
//         })
//     })

//     afterEach(async function () {
//         await driver.quit()
//     })
// })
